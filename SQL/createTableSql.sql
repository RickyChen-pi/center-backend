create table chat
(
    id         bigint auto_increment comment '聊天记录id'
        primary key,
    fromId     bigint                                  not null comment '发送消息id',
    toId       bigint                                  null comment '接收消息id',
    text       varchar(512) collate utf8mb4_unicode_ci null,
    chatType   tinyint                                 not null comment '聊天类型 1-私聊 2-群聊',
    createTime datetime default CURRENT_TIMESTAMP      null comment '创建时间',
    updateTime datetime default CURRENT_TIMESTAMP      null,
    teamId     bigint                                  null,
    isDelete   tinyint  default 0                      null
)
    comment '聊天消息表' collate = utf8mb4_general_ci
                         row_format = COMPACT;

create table follow
(
    id           bigint auto_increment comment '主键'
        primary key,
    userId       bigint            not null comment '用户id',
    followUserId bigint            not null comment '关注的用户id',
    createTime   datetime          null on update CURRENT_TIMESTAMP comment '创建时间',
    updateTime   datetime          null on update CURRENT_TIMESTAMP comment '更新时间',
    isDelete     tinyint default 0 null comment '逻辑删除'
)
    comment '用户关注表';

create table friends
(
    id         bigint auto_increment comment '主键'
        primary key,
    fromId     bigint                                  not null comment '发送申请的好友id',
    receiveId  bigint                                  null comment '接收申请的用户id',
    isRead     tinyint default 0                       not null comment '0-未读，1-已读',
    status     tinyint default 0                       not null comment '申请转态，0-未通过，1-已同意，2-已过期，3-不同意',
    createTime datetime                                null comment '创建时间',
    isDelete   tinyint default 0                       null comment '是否删除',
    remark     varchar(255) collate utf8mb4_general_ci null comment '好友申请备注信息'
)
    comment '好友申请表';

create table message
(
    id         bigint auto_increment comment '主键'
        primary key,
    type       tinyint           null comment '类型-1，点赞',
    fromId     bigint            null comment '消息发送的用户id',
    toId       bigint            null comment '消息接受的用户id',
    data       varchar(255)      null comment '消息内容',
    isRead     tinyint default 0 null comment '0-未读，1-已读',
    createTime datetime          null on update CURRENT_TIMESTAMP comment '创建时间',
    updateTime datetime          null on update CURRENT_TIMESTAMP comment '更新时间',
    isDelete   tinyint default 0 null comment '逻辑删除'
)
    comment '消息表';

create table sign
(
    id         bigint auto_increment comment '主键'
        primary key,
    userId     bigint            not null comment '用户id',
    signDate   datetime          null on update CURRENT_TIMESTAMP comment '签到时间',
    updateTime datetime          null on update CURRENT_TIMESTAMP comment '更新时间',
    createTime datetime          null on update CURRENT_TIMESTAMP comment '创建时间',
    isBackup   tinyint default 0 null comment '是否补签，0-不补签，1-补签'
)
    comment '签到表';

create table tag
(
    id         int auto_increment
        primary key,
    tagName    varchar(256) null comment '标签名称',
    userId     bigint       null comment '用户id',
    parentId   bigint       null comment '父标签id',
    isParent   tinyint      null comment '0-不是，1-父标签',
    createTime datetime     null on update CURRENT_TIMESTAMP comment '创建时间',
    updateTime datetime     null on update CURRENT_TIMESTAMP comment '更新时间',
    isDelete   tinyint      not null comment '是否删除',
    constraint uniIdx_tagName
        unique (tagName)
)
    comment '标签表';

create index idx_userId
    on tag (userId);

create table team
(
    id          bigint auto_increment comment '主键'
        primary key,
    name        varchar(255)                           not null comment '队伍名称',
    coverImage  varchar(255)                           null comment '封面图片',
    description varchar(1024)                          null comment '描述',
    joinNum     int                          default 1 null comment '队伍已加入人数',
    maxNum      int                                    not null comment '最大人数',
    expireTime  datetime                               null comment '过期时间',
    userId      bigint                                 null comment '队长ID',
    password    varchar(512)                           null comment '密码',
    status      int                                    not null comment '队伍状态：0-公开，1-私有，2-加密',
    createTime  datetime                               null on update CURRENT_TIMESTAMP comment '创建时间',
    updateTime  datetime                               null on update CURRENT_TIMESTAMP comment '修改时间',
    isDelete    tinyint(1) unsigned zerofill default 0 not null comment '是否删除'
);

create table user
(
    id           bigint auto_increment comment '用户编号'
        primary key,
    username     varchar(255)                 null comment '昵称',
    userAccount  varchar(255)                 null comment '登录账号',
    avatarUrl    varchar(255)                 null comment '头像',
    profile      varchar(512)                 null comment '个人简介',
    gender       tinyint                      null comment '性别 1:男  0:女',
    userPassword varchar(255)                 not null comment '密码',
    phone        varchar(128)                 null comment '电话',
    email        varchar(255)                 null comment '邮箱',
    status       int                          null comment '用户状态 0正常',
    role         tinyint                      null comment '用户角色，0：普通用户，1管理员',
    friendIds    varchar(255)                 null comment '好友编号',
    tags         varchar(1024)                null comment '标签列表',
    createTime   datetime                     null on update CURRENT_TIMESTAMP comment '创建时间',
    updateTime   datetime                     null on update CURRENT_TIMESTAMP comment '更新时间',
    isDelete     tinyint(1) unsigned zerofill null comment '是否删除 0正常 1 已删除'
);

create table user_team
(
    id         bigint auto_increment comment 'id'
        primary key,
    userId     bigint                             null comment '用户id',
    teamId     bigint                             null comment '队伍id',
    joinTime   datetime                           null comment '加入时间',
    createTime datetime default CURRENT_TIMESTAMP null comment '创建时间',
    updateTime datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    isDelete   tinyint  default 0                 not null comment '是否删除'
)
    comment '用户队伍关系';


