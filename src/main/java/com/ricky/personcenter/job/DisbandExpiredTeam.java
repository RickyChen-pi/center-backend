//package com.ricky.personcenter.job;
//
//import cn.hutool.core.date.DateUtil;
//import cn.hutool.core.date.LocalDateTimeUtil;
//import com.ricky.personcenter.model.entity.Team;
//import com.ricky.personcenter.service.TeamService;
//import lombok.extern.slf4j.Slf4j;
//import org.redisson.api.RLock;
//import org.redisson.api.RedissonClient;
//import org.springframework.scheduling.annotation.Scheduled;
//import org.springframework.stereotype.Component;
//
//import javax.annotation.Resource;
//import java.time.LocalDateTime;
//import java.util.Date;
//import java.util.List;
//import java.util.concurrent.TimeUnit;
//
///**
// * 解散已过期团队
// *
// * @author Ricky
// * @date 2024/02/17
// */
//@Slf4j
//@Component
//public class DisbandExpiredTeam {
//    @Resource
//    private TeamService teamService;
//    @Resource
//    private RedissonClient redissonClient;
//
//    /**
//     * 每天凌晨1点执行，解散已过期队伍
//     */
//    @Scheduled(cron = "* * 2 * * ?")
//    public void disbandTeam() {
//        RLock lock = redissonClient.getLock("personcenter:disband:team:lock");
//        try {
//            if (lock.tryLock(0,-1, TimeUnit.MILLISECONDS)) {
//                log.info("开始删除过期队伍");
//                List<Team> teamList = teamService.list();
//                String currentTime = DateUtil.now();
//                for (Team team : teamList) {
//                    Date expireTime = team.getExpireTime();
//                    String format = DateUtil.format(expireTime, "yyyy-MM-dd HH:mm:ss");
//                    if (DateUtil.parse(format).before(DateUtil.parse(currentTime))){
//                        //如果数据库时间早于当前时间，则执行删除
//                        teamService.deleteTeam(team.getId(), null, true);
//                    }
//                }
//            }
//        } catch (InterruptedException e) {
//            throw new RuntimeException(e);
//        } finally {
//            if (lock.isHeldByCurrentThread()){
//                lock.unlock();
//                System.out.println("unlock:" + Thread.currentThread().getId());
//            }
//        }
//    }
//}
