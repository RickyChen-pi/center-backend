package com.ricky.personcenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ricky.personcenter.model.entity.Sign;

/**
* @author Ricky
* @description 针对表【sign】的数据库操作Mapper
* @createDate 2024-02-07 16:45:00
* @Entity generator.domain.Sign
*/
public interface SignMapper extends BaseMapper<Sign> {

}




