package com.ricky.personcenter.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ricky.personcenter.model.entity.Message;

/**
* @author Ricky
* @description 针对表【message】的数据库操作Mapper
* @createDate 2024-02-07 16:45:00
* @Entity generator.domain.Message
*/
public interface MessageMapper extends BaseMapper<Message> {

}




