package com.ricky.personcenter.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ricky.personcenter.model.entity.Chat;

/**
* @author Ricky
* @description 针对表【follow】的数据库操作Mapper
* @createDate 2024-02-07 16:45:00
* @Entity generator.domain.Follow
*/
public interface ChatMapper extends BaseMapper<Chat> {

}




