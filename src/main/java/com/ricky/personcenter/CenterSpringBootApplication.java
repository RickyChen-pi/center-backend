package com.ricky.personcenter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableScheduling
@SpringBootApplication
@EnableRedisHttpSession
//@EnableAspectJAutoProxy
@MapperScan("com.ricky.personcenter.mapper")
public class CenterSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(CenterSpringBootApplication.class, args);
    }

}
