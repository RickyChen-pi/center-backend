package com.ricky.personcenter.service;

import cn.hutool.core.date.DateTime;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ricky.personcenter.model.entity.Chat;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.request.ChatRequest;
import com.ricky.personcenter.model.vo.ChatMessageVO;

import java.util.Date;
import java.util.List;

/**
 * @author Ricky
 * @date 2024/02/25
 */
public interface ChatService extends IService<Chat> {

    /**
     * 获取私聊
     *
     * @param chatRequest 聊天请求
     * @param privateChat 私聊
     * @param loginUser
     * @return {@link List}<{@link ChatMessageVO}>
     */
    List<ChatMessageVO> getPrivateChat(ChatRequest chatRequest, int privateChat, User loginUser);

    /**
     * 获取团队聊天
     *
     * @param chatRequest 聊天请求
     * @param loginUser   登录用户
     * @param chatType    聊天类型
     * @return {@link List}<{@link ChatMessageVO}>
     */
    List<ChatMessageVO> getTeamChat(ChatRequest chatRequest, int chatType, User loginUser);

    /**
     * 获取 Hall Chat
     *
     * @param hallChat  大厅聊天
     * @param loginUser 登录用户
     * @return {@link List}<{@link ChatMessageVO}>
     */
    List<ChatMessageVO> getHallChat(int hallChat, User loginUser);

    /**
     * @param key
     * @param id  编号
     */
    void deleteKey(String key, String id);

    ChatMessageVO chatResult(Long userId, Long toId, String text, int chatType, Date createTime);
}