package com.ricky.personcenter.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ricky.personcenter.model.dto.TeamqueryDTO;
import com.ricky.personcenter.model.entity.Team;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.request.*;
import com.ricky.personcenter.model.vo.TeamUserVO;
import com.ricky.personcenter.model.vo.TeamVO;
import com.ricky.personcenter.model.vo.UserVO;

import java.util.List;

/**
 * @author Ricky
 * @description 针对表【team】的数据库操作Service
 * @createDate 2024-01-15 11:19:47
 */
public interface TeamService extends IService<Team> {

    /**
     * 添加队伍
     *
     * @param team      团队
     * @param loginUser 登录用户
     * @return long
     */
    long addTeam(Team team, User loginUser);

    /**
     * 获取队伍列表
     *
     * @param teamqueryDTO TeamQuery DTO
     * @param isAdmin      是管理员
     * @return {@link List}<{@link TeamUserVO}>
     */
    Page<TeamVO> listTeams(long currentPage, TeamQueryRequest teamQueryRequest, boolean isAdmin);

    /**
     * 更新队伍信息
     *
     * @param teamUpdateRequest 更新请求信息
     * @param loginUser         登录用户
     * @return boolean
     */
    boolean updateTeam(TeamUpdateRequest teamUpdateRequest, User loginUser);

    /**
     * 加入团队
     *
     * @param teamJoinRequest 团队加入请求
     * @return boolean
     */
    boolean joinTeam(TeamJoinRequest teamJoinRequest, User loginUser);

    /**
     * 退出团队
     *
     * @param teamquitRequest Teamquit 请求
     * @param loginUser       登录用户
     * @return boolean
     */
    boolean quitTeam(TeamquitRequest teamquitRequest, User loginUser);

    /**
     * 解散队伍
     *
     * @param loginUser 登录用户
     * @param id        编号
     * @return boolean
     */
    boolean deleteTeam(long id, User loginUser, boolean isAdmin);

    /**
     * 列出我所有加入的队伍
     *
     * @param id 编号
     * @return {@link List}<{@link TeamVO}>
     */
    List<TeamVO> listAllMyJoin(Long id);

    /**
     * 获取已加入用户头像
     *
     * @param teamVoPage 团队 VO 页面
     * @return {@link Page}<{@link TeamVO}>
     */
    Page<TeamVO> getJoinedUserAvatar(Page<TeamVO> teamVoPage);

    /**
     * 列出我创建的队伍
     *
     * @param currentPage 当前页面
     * @param id          编号
     * @return {@link Page}<{@link TeamVO}>
     */
    Page<TeamVO> listMyCreate(long currentPage, Long id);

    /**
     * 获取团队
     *
     * @param teamId 团队 ID
     * @param userId 用户 ID
     * @return {@link TeamVO}
     */
    TeamVO getTeam(Long teamId, Long userId);

    /**
     * 列出我加入的队伍
     *
     * @param currentPage 当前页面
     * @param teamQuery   团队查询
     * @return {@link Page}<{@link TeamVO}>
     */
    Page<TeamVO> listMyJoin(long currentPage, TeamQueryRequest teamQuery);

    /**
     * 获取团队成员
     *
     * @param teamId      团队 ID
     * @param loginUserId 登录用户 ID
     * @return {@link List}<{@link UserVO}>
     */
    List<UserVO> getTeamMember(Long teamId, Long loginUserId);

    /**
     * 更改封面图片
     *
     * @param teamCoverUpdateRequest 团队封面更新请求
     * @param admin                  管理
     * @param userId                 用户 ID
     */
    void changeCoverImage(TeamCoverUpdateRequest teamCoverUpdateRequest, Long userId, boolean admin);

    /**
     * 踢出去
     *
     * @param teamId      团队 ID
     * @param userId      用户 ID
     * @param loginUserId 登录用户 ID
     * @param admin       管理
     */
    void kickOut(Long teamId, Long userId, Long loginUserId, boolean admin);

}
