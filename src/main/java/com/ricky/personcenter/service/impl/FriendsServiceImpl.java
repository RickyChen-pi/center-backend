package com.ricky.personcenter.service.impl;


import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ObjUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.ricky.personcenter.common.ErrorCode;
import com.ricky.personcenter.error.BusinessException;
import com.ricky.personcenter.mapper.FriendsMapper;
import com.ricky.personcenter.model.entity.Friends;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.request.FriendAddRequest;
import com.ricky.personcenter.model.vo.FriendsRecordVO;
import com.ricky.personcenter.service.FriendsService;
import com.ricky.personcenter.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static com.ricky.personcenter.contant.FriendConstant.*;
import static com.ricky.personcenter.contant.RedissonConstant.APPLY_LOCK;

/**
* @author Ricky
* @description 针对表【friends】的数据库操作Service实现
* @createDate 2024-02-07 16:45:00
*/
@Service
@Slf4j
public class FriendsServiceImpl extends ServiceImpl<FriendsMapper, Friends>
    implements FriendsService {

    @Resource
    private RedissonClient redissonClient;
    @Resource
    private UserService userService;

    /**
     * 添加好友
     *
     * @param friendAddRequest 好友添加请求
     * @param loginUser        登录用户
     * @return boolean
     */
    @Override
    public boolean addFriends(FriendAddRequest friendAddRequest, User loginUser) {

        if (StringUtils.isNotBlank(friendAddRequest.getRemark()) && friendAddRequest.getRemark().length() > 120) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"验证信息最多120个字符");
        }
        if (ObjectUtils.anyNull(loginUser.getId(),friendAddRequest.getReceiveId())){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"添加失败");
        }
        //添加的不能是自己
        if (ObjUtil.equals(loginUser.getId(), friendAddRequest.getReceiveId())) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"不能添加自己为好友");
        }


        RLock lock = redissonClient.getLock(APPLY_LOCK + loginUser.getId());

        try {
            //分布式锁
            //抢到锁之后立即执行，循环执行，
            if (lock.tryLock(0, -1, TimeUnit.MILLISECONDS)){
                //查询请求的用户id以及接收用户id 的记录存在并且大于等于1的时候就不能添加
                LambdaQueryWrapper<Friends> friendsLambdaQueryWrapper = new LambdaQueryWrapper<>();
                friendsLambdaQueryWrapper.eq(Friends::getReceiveId, friendAddRequest.getReceiveId());
                friendsLambdaQueryWrapper.eq(Friends::getFromId, loginUser.getId());
                List<Friends> list = this.list(friendsLambdaQueryWrapper);
                list.forEach(friends -> {
                    if (list.size() > 1 && friends.getStatus() == DEFAULT_STATUS) {
                        throw new BusinessException(ErrorCode.PARAMS_ERROR, "不能重复添加");
                    }
                });

                Friends newFriends = new Friends();
                newFriends.setFromId(loginUser.getId());
                newFriends.setReceiveId(friendAddRequest.getReceiveId());
                if (StringUtils.isBlank(friendAddRequest.getRemark())) {
                    newFriends.setRemark("我是" + userService.getById(loginUser.getId()).getUsername());
                } else {
                    newFriends.setRemark(friendAddRequest.getRemark());
                }
                newFriends.setCreateTime(new Date());
                return this.save(newFriends);
            }
        } catch (InterruptedException e) {
            log.error("joinFriend error", e);
            return false;
        } finally {
            //最后释放自己的锁
            if (lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
        return false;
    }

    /**
     * 查询添加好友的记录
     *
     * @param loginUser 登录用户
     * @return {@link List}<{@link FriendsRecordVO}>
     */
    @Override
    public List<FriendsRecordVO> getRecords(User loginUser) {

        LambdaQueryWrapper<Friends> friendsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        friendsLambdaQueryWrapper.eq(Friends::getReceiveId, loginUser.getId());

        List<Friends> friendsList = this.list(friendsLambdaQueryWrapper);

        Collections.reverse(friendsList);
        return friendsList.stream().map(friends -> {
            FriendsRecordVO friendsRecordVO = new FriendsRecordVO();
            BeanUtils.copyProperties(friends, friendsRecordVO);
            User user = userService.getById(friends.getFromId());
            friendsRecordVO.setApplyUser(userService.getSafetyUser(user));
            return friendsRecordVO;
        }).collect(Collectors.toList());

    }

    /**
     * 同意申请
     *
     * @param loginUser 登录用户
     * @param fromId    从 ID
     * @return boolean
     */
    @Override
    public boolean agreeToApply(User loginUser, Long fromId) {
        LambdaQueryWrapper<Friends> friendsLambdaQueryWrapper = new LambdaQueryWrapper<>();
        friendsLambdaQueryWrapper.eq(Friends::getReceiveId, loginUser.getId());
        friendsLambdaQueryWrapper.eq(Friends::getFromId, fromId);
        List<Friends> recordCount = this.list(friendsLambdaQueryWrapper);

        //筛选申请状态为0-未通过 并且 申请时间为过期的申请（7天）
        List<Friends> collect = recordCount.stream()
                .filter(friends -> friends.getStatus() == DEFAULT_STATUS && (DateUtil.between(new Date(), friends.getCreateTime(), DateUnit.DAY) <= 7))
                .collect(Collectors.toList());

        if (collect.isEmpty()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "该申请不存在或已过期");
        }
        //多线程环境中以原子方式更新 boolean 类型值
        AtomicBoolean flag = new AtomicBoolean(false);

        collect.forEach(friends -> {
            //分别查询receiveId 和 fromId的用户，之后更改userId中的数据
            User receiveUser = userService.getById(loginUser.getId());
            User fromUser = userService.getById(fromId);

            Set<Long> receiveUserIds = com.ricky.personcenter.utils.StringUtils.stringJsonListToLongSet(receiveUser.getFriendIds());
            Set<Long> fromUserIds = com.ricky.personcenter.utils.StringUtils.stringJsonListToLongSet(fromUser.getFriendIds());

            fromUserIds.add(receiveUser.getId());
            receiveUserIds.add(fromUser.getId());

            Gson gson = new Gson();
            String jsonFromUserIds = gson.toJson(fromUserIds);
            String jsonReceiveUserIds = gson.toJson(receiveUserIds);
            receiveUser.setFriendIds(jsonReceiveUserIds);
            fromUser.setFriendIds(jsonFromUserIds);

            //修改状态由0 -> 1
            friends.setStatus(AGREE_STATUS);
            //以原子方式更改数据
            flag.set(userService.updateById(fromUser) && userService.updateById(receiveUser) && this.updateById(friends));
        });
        return flag.get();
    }

    /**
     * 拒绝申请
     *
     * @param loginUser 登录用户
     * @param id        编号
     * @return boolean
     */
    @Override
    public boolean canceledApply(User loginUser, Long id) {
        QueryWrapper<Friends> friendsQueryWrapper = new QueryWrapper<>();
        friendsQueryWrapper.eq("fromId", id);
        Friends friend = this.getOne(friendsQueryWrapper);
        if (friend.getStatus() != DEFAULT_STATUS) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "该申请已通过");
        }
        if (DateUtil.between(new Date(), friend.getCreateTime(), DateUnit.DAY) >= 3 || friend.getStatus() == EXPIRED_STATUS) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "该申请已过期");
        }
        friend.setStatus(REVOKE_STATUS);
        return this.updateById(friend);
    }

}




