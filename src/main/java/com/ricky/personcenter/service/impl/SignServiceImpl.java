package com.ricky.personcenter.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ricky.personcenter.mapper.SignMapper;
import com.ricky.personcenter.model.entity.Sign;
import com.ricky.personcenter.service.SignService;
import org.springframework.stereotype.Service;

/**
* @author Ricky
* @description 针对表【sign】的数据库操作Service实现
* @createDate 2024-02-07 16:45:00
*/
@Service
public class SignServiceImpl extends ServiceImpl<SignMapper, Sign>
    implements SignService {

}




