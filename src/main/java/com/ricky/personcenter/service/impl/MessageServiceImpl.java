package com.ricky.personcenter.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ricky.personcenter.mapper.MessageMapper;
import com.ricky.personcenter.model.entity.Message;
import com.ricky.personcenter.service.MessageService;
import org.springframework.stereotype.Service;

/**
* @author Ricky
* @description 针对表【message】的数据库操作Service实现
* @createDate 2024-02-07 16:45:00
*/
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message>
    implements MessageService {

    /**
     * 有新消息
     *
     * @param userId 用户 ID
     * @return {@link Boolean}
     */
    @Override
    public Boolean hasNewMessage(Long userId) {
        //todo

        return null;
    }

    /**
     * 获取新消息数量
     *
     * @param loginId 登录 ID
     * @return long
     */
    @Override
    public long getMessageNum(Long loginId) {
        LambdaQueryWrapper<Message> messageLambdaQueryWrapper = new LambdaQueryWrapper<>();
        messageLambdaQueryWrapper.eq(Message::getToId, loginId).eq(Message::getIsRead, 0);
        return this.count(messageLambdaQueryWrapper);
    }
}




