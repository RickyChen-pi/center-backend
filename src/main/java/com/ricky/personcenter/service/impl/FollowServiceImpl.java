package com.ricky.personcenter.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ricky.personcenter.model.entity.Follow;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.vo.UserVO;
import com.ricky.personcenter.service.FollowService;
import com.ricky.personcenter.mapper.FollowMapper;
import com.ricky.personcenter.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
* @author Ricky
* @description 针对表【follow】的数据库操作Service实现
* @createDate 2024-02-07 16:45:00
*/
@Service
public class FollowServiceImpl extends ServiceImpl<FollowMapper, Follow>
    implements FollowService{

    @Resource
    @Lazy
    private UserService userService;

    /**
     * 关注用户
     *
     * @param userId      用户 ID
     * @param loginUserId 登录用户 ID
     */
    @Override
    public void followUser(Long userId, Long loginUserId) {
        LambdaQueryWrapper<Follow> followLambdaQueryWrapper = new LambdaQueryWrapper<>();
        followLambdaQueryWrapper.eq(Follow::getFollowUserId,userId).eq(Follow::getUserId, loginUserId);
        synchronized (this) {
            long count = this.count(followLambdaQueryWrapper);
            if (count == 0) {
                Follow follow = new Follow();
                follow.setFollowUserId(userId);
                follow.setUserId(loginUserId);
                this.save(follow);
            } else {
                this.remove(followLambdaQueryWrapper);
            }
        }
    }

    /**
     * 列出我关注的用户
     *
     * @param loginUserId 登录用户 ID
     * @return {@link List}<{@link UserVO}>
     */
    @Override
    public List<UserVO> listMyFollow(Long loginUserId) {

        LambdaQueryWrapper<Follow> followLambdaQueryWrapper = new LambdaQueryWrapper<>();
        followLambdaQueryWrapper.eq(Follow::getUserId, loginUserId);
        List<Follow> list = this.list(followLambdaQueryWrapper);

        List<User> userList = list.stream()
                .map(follow -> userService.getById(follow.getFollowUserId()))
                .collect(Collectors.toList());

        return userList.stream().map(user -> {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(user, userVO);
            userVO.setIsFollow(true);
            return userVO;
        }).collect(Collectors.toList());
    }

    /**
     * 获取粉丝名单
     * 关注我的人以及信息
     * @param loginUserId 登录用户 ID
     * @return {@link List}<{@link UserVO}>
     */
    @Override
    public List<UserVO> listFans(Long loginUserId) {
        LambdaQueryWrapper<Follow> followLambdaQueryWrapper = new LambdaQueryWrapper<>();
        followLambdaQueryWrapper.eq(Follow::getFollowUserId, loginUserId);
        List<Follow> list = this.list(followLambdaQueryWrapper);
        if (list == null || list.isEmpty() ) {
            return new ArrayList<>();
        }

        //获取我的粉丝信息
        List<User> userList = list.stream()
                .map((follow -> userService.getById(follow.getUserId())))
                .collect(Collectors.toList());

        return userList.stream().map(item -> {
            UserVO userVO = new UserVO();
            BeanUtils.copyProperties(item, userVO);
            LambdaQueryWrapper<Follow> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(Follow::getUserId,loginUserId).eq(Follow::getFollowUserId,item.getId());
            long count = this.count(lambdaQueryWrapper);
            userVO.setIsFollow(count > 0);
            return userVO;
        }).collect(Collectors.toList());
    }

    /**
     * 获取用户关注信息
     *
     * @param user        用户
     * @param uerId 登录用户 ID
     * @return {@link UserVO}
     */
    @Override
    public UserVO getUserFollowInfo(User user, Long userId) {
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        LambdaQueryWrapper<Follow> followLambdaQueryWrapper = new LambdaQueryWrapper<>();
        followLambdaQueryWrapper.eq(Follow::getUserId, userId)
                .eq(Follow::getFollowUserId, userVO.getId());
        long count = this.count(followLambdaQueryWrapper);
        userVO.setIsFollow(count > 0);
        return userVO;

    }
}




