package com.ricky.personcenter.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ricky.personcenter.mapper.TagMapper;
import com.ricky.personcenter.model.entity.Tag;
import com.ricky.personcenter.service.TagService;
import org.springframework.stereotype.Service;

/**
* @author Ricky
* @description 针对表【tag】的数据库操作Service实现
* @createDate 2024-02-07 16:45:00
*/
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag>
    implements TagService {

}




