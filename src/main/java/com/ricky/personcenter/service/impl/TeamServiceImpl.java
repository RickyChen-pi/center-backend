package com.ricky.personcenter.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.unit.DataUnit;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.base.Objects;
import com.ricky.personcenter.common.ErrorCode;
import com.ricky.personcenter.error.BusinessException;
import com.ricky.personcenter.mapper.TeamMapper;
import com.ricky.personcenter.model.entity.Team;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.entity.UserTeam;
import com.ricky.personcenter.model.enums.TeamStatusEnum;
import com.ricky.personcenter.model.request.*;
import com.ricky.personcenter.model.vo.TeamUserVO;
import com.ricky.personcenter.model.vo.TeamVO;
import com.ricky.personcenter.model.vo.UserVO;
import com.ricky.personcenter.service.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author Ricky
 * @description 针对表【team】的数据库操作Service实现
 * @createDate 2024-01-15 11:19:47
 */
@Slf4j
@Service
public class TeamServiceImpl extends ServiceImpl<TeamMapper, Team>
        implements TeamService {

    @Resource
    private UserTeamService userTeamService;

    @Resource
    private UserService userService;

    @Resource
    private RedissonClient redissonClient;

    @Resource
    private FollowService followService;
    @Resource
    private FileService fileService;

    /**
     * 创建团队
     *
     * @param team      团队
     * @param loginUser 登录用户
     * @return long
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public long addTeam(Team team, User loginUser) {
        //1、判断请求参数是否为空
        if (team == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Date newExpireTime = team.getExpireTime();
        if (DateUtil.isSameDay(newExpireTime, DateUtil.date())){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"不能选择当天时间");
        }
        //2、是否登录，未登录不允许建队伍
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN,"未登录");
        }
        final Long userId = loginUser.getId();
        //3、校验信息
        //1.队伍人数 >1 && <= 20
        int maxNum = Optional.ofNullable(team.getMaxNum()).orElse(0);
        if (maxNum < 1 || maxNum > 20) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "队伍人数不满足要求");
        }
        //2.队伍标题 <= 20
        String name = team.getName();
        if (StringUtils.isBlank(name) || name.length() > 20) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "队伍标题不满足要求");
        }
        //3.队伍描述 <= 512
        String description = team.getDescription();
        if (StringUtils.isNotBlank(description) && description.length() > 512) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "队伍描述过长");
        }
        //4.status 是否公开，不公开默认0
        int status = Optional.ofNullable(team.getStatus()).orElse(0);
        TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(status);
        if (statusEnum == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "队伍状态不满足");
        }
        //5.如果 status 是加密状态，一定要有密码，并且密码 <= 32
        String password = team.getPassword();
        if (TeamStatusEnum.SECRET.equals(statusEnum) && (StringUtils.isBlank(password) || password.length() > 32)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码不正确");
        }
        //6. 超时时间 > 当前时间
        Date expireTime = team.getExpireTime();
        if (new Date().after(expireTime)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "超时时间 > 当前时间");
        }

        //7.校验用户最多只能创建 5 个队伍
        QueryWrapper<Team> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userId", userId);
        long hasTeamNum = this.count(queryWrapper);
        if (hasTeamNum >= 5) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户最多只能创建5个队伍");
        }

        //8、插入队伍信息到队伍表中
        //（暂解决）存在bug，可能用户疯狂点击创建好多个队伍-->解决方法：加锁
        //add 单机锁
        synchronized (this){
            team.setId(null);
            team.setUserId(userId);
            team.setCreateTime(new Date());
            team.setUpdateTime(new Date());
            boolean result = this.save(team);
            Long teamId = team.getId();
            if (!result || teamId == null) {
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "创建队伍失败");
            }
            // 9. 插入用户  => 队伍关系到关系表
            UserTeam userTeam = new UserTeam();
            userTeam.setUserId(userId);
            userTeam.setTeamId(teamId);
            userTeam.setJoinTime(new Date());
            result = userTeamService.save(userTeam);
            if (!result) {
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "创建队伍失败");
            }
            return teamId;
        }
    }

    /**
     * 列出队伍
     *
     * @param teamqueryDTO TeamQuery DTO
     * @param isAdmin      是管理员
     * @return {@link List}<{@link TeamUserVO}>
     */
    @Override
    public Page<TeamVO> listTeams(long currentPage, TeamQueryRequest teamQueryRequest, boolean isAdmin) {

        LambdaQueryWrapper<Team> teamLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //组合查询条件
        if (teamQueryRequest != null ) {
            Long id = teamQueryRequest.getId();
            List<Long> idList = teamQueryRequest.getIdList();
            String searchText = teamQueryRequest.getSearchText();
            //根据队伍名查询
            String name = teamQueryRequest.getName();
            //根据描述查询
            String description = teamQueryRequest.getDescription();
            //根据最大人数查询
            Integer maxNum = teamQueryRequest.getMaxNum();
            //根据队长id查询
            Long userId = teamQueryRequest.getUserId();
            //根据状态查询
            Integer status = teamQueryRequest.getStatus();
            TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(status);
            if (statusEnum == null) {
                statusEnum = TeamStatusEnum.PUBLIC;
            }
            if (!isAdmin && statusEnum.equals(TeamStatusEnum.PRIVATE)) {
                throw new BusinessException(ErrorCode.NO_AUTH);
            }
            teamLambdaQueryWrapper
                    .eq(id != null && id > 0, Team::getId, id)
                    .in(CollectionUtils.isNotEmpty(idList), Team::getId, idList)
                    .and(StringUtils.isNotBlank(searchText), qw -> qw.like(Team::getName, searchText)
                            .or()
                            .like(Team::getDescription, searchText)
                    )
                    .like(StringUtils.isNotBlank(name), Team::getName, name)
                    .like(StringUtils.isNotBlank(description), Team::getDescription, description)
                    .eq(maxNum != null && maxNum > 0, Team::getMaxNum, maxNum)
                    .eq(userId != null && userId > 0, Team::getUserId, userId)
                    .eq(Team::getStatus, statusEnum.getValue());
        }
        //不展示已过期的队伍
        teamLambdaQueryWrapper.and(qw -> qw.gt(Team::getExpireTime, new Date()).or().isNull(Team::getExpireTime));
        return listTeamByCondition(currentPage, teamLambdaQueryWrapper);

    }

    /**
     * 按条件列出队伍
     *
     * @param currentPage            当前页面
     * @param teamLambdaQueryWrapper 团队 Lambda 查询包装器
     * @return {@link List}<{@link TeamUserVO}>
     */
    public Page<TeamVO> listTeamByCondition(long currentPage, LambdaQueryWrapper<Team> teamLambdaQueryWrapper) {
        Page<Team> teamPage = this.page(new Page<>(currentPage, 8), teamLambdaQueryWrapper);
        if (CollectionUtils.isEmpty(teamPage.getRecords())) {
            return new Page<>();
        }
        Page<TeamVO> teamVoPage = new Page<>();
        // 关联查询创建人的用户信息
        BeanUtils.copyProperties(teamPage, teamVoPage, "records");
        List<Team> teamPageRecords = teamPage.getRecords();
        ArrayList<TeamVO> teamUserVOList = new ArrayList<>();
        for (Team team : teamPageRecords) {
            Long userId = team.getUserId();
            if (userId == null) {
                continue;
            }
            User user = userService.getById(userId);
            TeamVO teamUserVO = new TeamVO();
            BeanUtils.copyProperties(team, teamUserVO);
            // 脱敏用户信息
            if (user != null) {
                UserVO userVO = new UserVO();
                BeanUtils.copyProperties(user, userVO);
                teamUserVO.setCreateUser(userVO);
            }
            teamUserVOList.add(teamUserVO);
        }
        teamVoPage.setRecords(teamUserVOList);
        return teamVoPage;
    }

    /**
     * 修改队伍信息
     *
     * @param teamUpdateRequest 更改队伍信息请求
     * @param loginUser         登录用户
     * @return boolean
     */
    @Override
    public boolean updateTeam(TeamUpdateRequest teamUpdateRequest, User loginUser) {
        if (teamUpdateRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Long id = teamUpdateRequest.getId();
        if (id == null || id <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        Date newExpireTime = teamUpdateRequest.getExpireTime();
        if (DateUtil.isSameDay(newExpireTime, DateUtil.date())){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"不能选择当天时间");
        }
        //查询队伍是否存在
        Team oldTeam = this.getById(id);
        if (oldTeam == null){
            throw new BusinessException(ErrorCode.NULL_ERROR,"队伍不存在");
        }

        // 只有队伍创建者和管理员才能修改队伍信息
        if (!oldTeam.getUserId().equals(loginUser.getId()) && !userService.isAdmin(loginUser)){
            throw new BusinessException(ErrorCode.NO_AUTH);
        }

        //如果队伍状态为加密，必须要有密码
        TeamStatusEnum statusEnum = TeamStatusEnum.getEnumByValue(teamUpdateRequest.getStatus());
        if (statusEnum.equals(TeamStatusEnum.SECRET) && (StringUtils.isBlank(teamUpdateRequest.getPassword()))){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"加密房间必须要设置密码");
        }
        if (statusEnum.equals(TeamStatusEnum.PUBLIC) && (StringUtils.isNotBlank(teamUpdateRequest.getPassword()))){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"非加密房间不需要设置密码");
        }
        if (statusEnum.equals(TeamStatusEnum.PRIVATE) && (StringUtils.isNotBlank(teamUpdateRequest.getPassword()))){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"非加密房间不需要设置密码");
        }

        Team updateTeam = new Team();
        BeanUtils.copyProperties(teamUpdateRequest,updateTeam);
        return this.updateById(updateTeam);
    }

    /**
     * 加入团队
     *
     * @param teamJoinRequest 团队加入请求
     * @return boolean
     */
    @SneakyThrows(Exception.class)
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean joinTeam(TeamJoinRequest teamJoinRequest, User loginUser) {
        if (teamJoinRequest == null ){
            throw new BusinessException(ErrorCode.NULL_ERROR);
        }
        Long teamId = teamJoinRequest.getTeamId();
        if (teamId == null || teamId <= 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍不存在");
        }
        //查询队伍信息
        Team team = getTeamById(teamId);
        Date expireTime = team.getExpireTime();
        if (expireTime != null && expireTime.before(new Date())) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍已过期");
        }
        Integer status = team.getStatus();
        TeamStatusEnum teamStatusEnum = TeamStatusEnum.getEnumByValue(status);
        if (TeamStatusEnum.PRIVATE.equals(teamStatusEnum)){
            throw new BusinessException(ErrorCode.NO_AUTH,"禁止加入私有队伍");
        }
        String password = teamJoinRequest.getPassword();
        if (TeamStatusEnum.SECRET.equals(teamStatusEnum)){
            if (StringUtils.isBlank(password) || !password.equals(team.getPassword())){
                throw new BusinessException(ErrorCode.PARAMS_ERROR,"密码不正确");
            }
        }
        //该用户已加入的队伍数量
        Long userId = loginUser.getId();
        //使用redisson实现只有一个线程能拿到锁
        RLock lock = redissonClient.getLock("personcenter:join_team");
        boolean isLock = false;
        isLock = lock.tryLock(5,10,TimeUnit.SECONDS);
        if (!isLock) {
            log.error("获取加入队伍锁失败");
            throw new BusinessException(ErrorCode.NULL_RESPONSE,"加入队伍失败");
        }

        try {
            //业务逻辑
            QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
            userTeamQueryWrapper.eq("userId", userId);
            long hasJoinNum = userTeamService.count(userTeamQueryWrapper);
            if (hasJoinNum > 5) {
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "最多创建和加入5个队伍");
            }

            //(已解决) 存在重复添加的情况：用户疯狂点击的情况下；解决：需要加锁
            //不能重复加入已加入的队伍
            userTeamQueryWrapper = new QueryWrapper<>();
            userTeamQueryWrapper.eq("userId", userId);
            userTeamQueryWrapper.eq("teamId", teamId);
            long hasUserJoinTeam = userTeamService.count(userTeamQueryWrapper);
            if (hasUserJoinTeam > 0) {
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "不能重复加入已加入的队伍");
            }

            Integer joinNum = team.getJoinNum();
            if (joinNum >= team.getMaxNum()) {
                throw new BusinessException(ErrorCode.PARAMS_ERROR, "队伍已满");
            }
            joinNum+=1;
            team.setJoinNum(joinNum);
            this.updateById(team);

            //修改队伍信息
            UserTeam userTeam = new UserTeam();
            userTeam.setUserId(userId);
            userTeam.setTeamId(teamId);
            userTeam.setJoinTime(new Date());
            return userTeamService.save(userTeam);
        }  finally {
            //判断这个锁是否是当前线程，只能释放自己的锁
            if (lock.isHeldByCurrentThread()){
                lock.unlock();
                System.out.println("unlock:" + Thread.currentThread().getId());
            }
        }
    }

    /**
     * 退出团队
     *
     * @param teamquitRequest  请求
     * @param loginUser       登录用户
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean quitTeam(TeamquitRequest teamquitRequest, User loginUser) {
        if (teamquitRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Long teamId = teamquitRequest.getTeamId();
        if (teamId == null || teamId <= 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team team = getTeamById(teamId);
        Long userId = loginUser.getId();
        UserTeam queryUserTeam = new UserTeam();
        queryUserTeam.setUserId(userId);
        queryUserTeam.setTeamId(teamId);
        QueryWrapper<UserTeam> queryWrapper = new QueryWrapper<>(queryUserTeam);
        long count = userTeamService.count(queryWrapper);
        if (count == 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "未加入队伍");
        }
        //已加入队伍的人数
        Integer teamHasJoinNum = team.getJoinNum();


        int joinNum = teamHasJoinNum - 1;
        //队伍仅剩一人，解散队伍
        if (teamHasJoinNum == 1){
            //删除队伍以及所有加入队伍的关系
            this.removeById(teamId);
        }else {
            //队伍至少有两人
            //是队长
            if (team.getUserId().equals(userId)){
                //把队伍转移到最早加入的成员
                //1、查询已加入队伍的所有用户和加入时间
                QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
                userTeamQueryWrapper.eq("teamId", teamId);
                userTeamQueryWrapper.last("order by id asc limit 2");
                List<UserTeam> userTeamList = userTeamService.list(userTeamQueryWrapper);
                if (CollectionUtils.isEmpty(userTeamList) || userTeamList.size() <= 1){
                    throw new BusinessException(ErrorCode.SYSTEM_ERROR);
                }
                UserTeam nextUserTeam = userTeamList.get(1);
                Long nextTeamLeadId = nextUserTeam.getUserId();
                //更新当前队伍的队长
                Team updateTeam = new Team();
                updateTeam.setId(teamId);
                updateTeam.setUserId(nextTeamLeadId);
                updateTeam.setJoinNum(joinNum);
                boolean result = this.updateById(updateTeam);
                if (!result){
                    throw new BusinessException(ErrorCode.SYSTEM_ERROR,"更新队长失败");
                }
            }
        }
        Team quitTeam = new Team();
        quitTeam.setJoinNum(joinNum);
        quitTeam.setId(teamId);
        this.updateById(quitTeam);

        //删除关系
        return userTeamService.remove(queryWrapper);
    }

    /**
     * 解散队伍
     *
     * @param id 团队 ID
     * @return boolean
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteTeam(long teamId, User loginUser, boolean isAdmin) {
        //校验队伍是否勋在
        Team team = getTeamById(teamId);
        //校验是否是队长
        if (isAdmin){
            //移除所有加入队伍的信息
            QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
            userTeamQueryWrapper.eq("teamId", teamId);
            boolean remove = userTeamService.remove(userTeamQueryWrapper);
            if (!remove) {
                throw new BusinessException(ErrorCode.SYSTEM_ERROR,"删除队伍关联信息失败");
            }
            return this.removeById(teamId);
        }
        if (!team.getUserId().equals(loginUser.getId())) {
            throw new BusinessException(ErrorCode.NO_AUTH, "无权限访问");
        }
        // 移除所有加入队伍的关联信息
        QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
        userTeamQueryWrapper.eq("teamId", teamId);
        boolean result = userTeamService.remove(userTeamQueryWrapper);
        if (!result) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "删除队伍关联信息失败");
        }
        // 删除队伍
        return this.removeById(teamId);
    }

    /**
     * 列出我所有加入的队伍
     *
     * @param id 编号
     * @return {@link List}<{@link TeamVO}>
     */
    @Override
    public List<TeamVO> listAllMyJoin(Long id) {
        LambdaQueryWrapper<UserTeam> userTeamLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userTeamLambdaQueryWrapper.eq(UserTeam::getUserId,id);
        List<Long> teamIds = userTeamService.list(userTeamLambdaQueryWrapper)
                .stream()
                .map(UserTeam::getTeamId)
                .collect(Collectors.toList());

        LambdaQueryWrapper<Team> teamLambdaQueryWrapper = new LambdaQueryWrapper<>();
        teamLambdaQueryWrapper.in(Team::getId, teamIds);
        List<Team> teamList = this.list(teamLambdaQueryWrapper);
        return teamList.stream().map((team) -> {
            TeamVO teamVO = new TeamVO();
            BeanUtils.copyProperties(team,teamVO);
            teamVO.setHasJoin(true);
            return teamVO;
        }).collect(Collectors.toList());

    }

    /**
     * 获取已加入用户头像
     *
     * @param teamVoPage 团队 VO 页面
     * @return {@link Page}<{@link TeamVO}>
     */
    @Override
    public Page<TeamVO> getJoinedUserAvatar(Page<TeamVO> teamVoPage) {
        teamVoPage.getRecords().forEach((item) -> {
            Long teamId = item.getId();
            LambdaQueryWrapper<UserTeam> userTeamLambdaQueryWrapper = new LambdaQueryWrapper<>();
            userTeamLambdaQueryWrapper.eq(UserTeam::getTeamId, teamId);
            // todo
            List<Long> joinedUserIdList = userTeamService.list(userTeamLambdaQueryWrapper)
                    .stream().map((UserTeam::getUserId))
                    .limit(3)
                    .collect(Collectors.toList());

            List<String> joinedUserAvatarList = userService.listByIds(joinedUserIdList)
                    .stream().map((User::getAvatarUrl))
                    .collect(Collectors.toList());
            item.setJoinedUserAvatars(joinedUserAvatarList);
        });
        return teamVoPage;
    }

    /**
     * 列出我创建的队伍
     *
     * @param currentPage 当前页面
     * @param id          编号
     * @return {@link Page}<{@link TeamVO}>
     */
    @Override
    public Page<TeamVO> listMyCreate(long currentPage, Long userId) {
        LambdaQueryWrapper<Team> teamLambdaQueryWrapper = new LambdaQueryWrapper<>();
        teamLambdaQueryWrapper.eq(Team::getUserId, userId);
        Page<Team> teamPage = this.page(new Page<>(currentPage, 8), teamLambdaQueryWrapper);
        List<TeamVO> teamVOList = teamPage.getRecords()
                .stream().map(team -> this.getTeam(team.getId(), userId))
                .collect(Collectors.toList());
        Page<TeamVO> teamVOPage = new Page<>();
        BeanUtils.copyProperties(teamPage,teamVOPage);
        teamVOPage.setRecords(teamVOList);
        return teamVOPage;
    }

    /**
     * 获取团队
     *
     * @param teamId 团队 ID
     * @param userId 用户 ID
     * @return {@link TeamVO}
     */
    @Override
    public TeamVO getTeam(Long teamId, Long userId) {
        Team team = this.getById(teamId);
        if (team == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍不存在");
        }
        TeamVO teamVO = new TeamVO();
        BeanUtils.copyProperties(team,teamVO);
        LambdaQueryWrapper<UserTeam> userTeamLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userTeamLambdaQueryWrapper.eq(UserTeam::getTeamId, teamId);
        //查询队伍人数
        //long count = userTeamService.count(userTeamLambdaQueryWrapper);
        userTeamLambdaQueryWrapper.eq(UserTeam::getUserId, userId);
        long userJoin = userTeamService.count(userTeamLambdaQueryWrapper);
        teamVO.setHasJoin(userJoin > 0);
        User leader = userService.getById(team.getUserId());
        teamVO.setLeaderName(leader.getUsername());
        return teamVO;
    }

    /**
     * 列出我加入的队伍
     *
     * @param currentPage 当前页面
     * @param teamQuery   团队查询
     * @return {@link Page}<{@link TeamVO}>
     */
    @Override
    public Page<TeamVO> listMyJoin(long currentPage, TeamQueryRequest teamQuery) {
        List<Long> idList = teamQuery.getIdList();
        LambdaQueryWrapper<Team> teamLambdaQueryWrapper = new LambdaQueryWrapper<>();
        teamLambdaQueryWrapper.in(Team::getId, idList);
        return listTeamByCondition(currentPage,teamLambdaQueryWrapper);

    }

    /**
     * 获取团队成员
     *
     * @param teamId      团队 ID
     * @param loginUserId 登录用户 ID
     * @return {@link List}<{@link UserVO}>
     */
    @Override
    public List<UserVO> getTeamMember(Long teamId, Long userId) {
        Team team = this.getById(teamId);
        if (team == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍不存在");
        }
        LambdaQueryWrapper<UserTeam> userTeamLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userTeamLambdaQueryWrapper.eq(UserTeam::getTeamId, teamId);
        List<UserTeam> userTeamList = userTeamService.list(userTeamLambdaQueryWrapper);
        List<Long> userIdList = userTeamList.stream()
                .map(UserTeam::getUserId)
                .filter(id -> !Objects.equal(id, userId))
                .collect(Collectors.toList());
        if (userIdList.isEmpty()) {
            return new ArrayList<>();
        }
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.in(User::getId, userIdList);
        List<User> userList = userService.list(userLambdaQueryWrapper);
        return userList.stream().map(user -> followService.getUserFollowInfo(user,userId)).collect(Collectors.toList());
    }

    /**
     * 更改封面图片
     *
     * @param userId  用户 ID
     * @param admin   管理
     * @param request 请求
     */
    @Override
    public void changeCoverImage(TeamCoverUpdateRequest request, Long userId, boolean admin) {
        MultipartFile image = request.getFile();
        if (image == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Long teamId = request.getId();
        if (teamId == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team team = this.getById(teamId);
        if (team == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        if (!team.getUserId().equals(userId) && !admin) {
            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        String fileName = fileService.uploadFileAvatar(image);
        Team temp = new Team();
        temp.setId(team.getId());
        temp.setCoverImage(fileName);
        this.updateById(temp);

    }

    /**
     * 踢出去
     *
     * @param teamId      团队 ID
     * @param userId      用户 ID
     * @param loginUserId 登录用户 ID
     * @param admin       管理
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void kickOut(Long teamId, Long userId, Long loginUserId, boolean admin) {
        if (userId.equals(loginUserId)){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"不能将自己踢出");
        }
        Team team = this.getById(teamId);
        if (team == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍不存在");
        }
        Integer joinNum = team.getJoinNum();
        if (!team.getUserId().equals(loginUserId) && !admin) {
            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        LambdaQueryWrapper<UserTeam> userTeamLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userTeamLambdaQueryWrapper.eq(UserTeam::getTeamId,teamId)
                .eq(UserTeam::getUserId, userId);

        userTeamService.remove(userTeamLambdaQueryWrapper);
        joinNum --;
        team.setId(teamId);
        team.setJoinNum(joinNum);
        this.updateById(team);
    }


    /**
     * 按队伍 ID 对已加入队伍的人数进行计数
     *
     * @param teamId 团队 ID
     * @return long
     */
    private long countTeamUsersByTeamId(long teamId){
        QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
        userTeamQueryWrapper.eq(" teamId",teamId);
        return userTeamService.count(userTeamQueryWrapper);
    }

    /**
     * 判断队伍是否存在/队伍存在则查询数据
     *
     * @param teamId 团队 ID
     * @return {@link Team}
     */
    private Team getTeamById(Long teamId) {
        if (teamId == null || teamId <= 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍不存在");
        }
        Team team = this.getById(teamId);
        if (team == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"队伍不存在");
        }
        return team;
    }
}






