package com.ricky.personcenter.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ricky.personcenter.common.ErrorCode;
import com.ricky.personcenter.error.BusinessException;
import com.ricky.personcenter.mapper.ChatMapper;
import com.ricky.personcenter.model.entity.Chat;
import com.ricky.personcenter.model.entity.Team;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.request.ChatRequest;
import com.ricky.personcenter.model.vo.ChatMessageVO;
import com.ricky.personcenter.model.vo.WebSocketVO;
import com.ricky.personcenter.service.ChatService;
import com.ricky.personcenter.service.TeamService;
import com.ricky.personcenter.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.ricky.personcenter.contant.ChatConstant.*;
import static com.ricky.personcenter.contant.RedisConstants.*;
import static com.ricky.personcenter.contant.UserConstant.ADMIN_ROLE;

/**
 * @author Ricky
 * @date 2024/02/25
 */
@Service
public class ChatServiceImpl extends ServiceImpl<ChatMapper, Chat>
     implements ChatService {

    @Resource
    private UserService userService;
    @Resource
    private TeamService teamService;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;


    /**
     * 获取私聊
     *
     * @param chatRequest 聊天请求
     * @param loginUser   登录用户
     * @param chatType    聊天类型
     * @return {@link List}<{@link ChatMessageVO}>
     */
    @Override
    public List<ChatMessageVO> getPrivateChat(ChatRequest chatRequest, int chatType, User loginUser) {
        Long toId = chatRequest.getToId();
        if (toId == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        //获取缓存中的信息
        List<ChatMessageVO> chatRecords = getCache(CACHE_CHAT_HALL, loginUser.getId() + String.valueOf(toId));
        //当缓存中有记录
        if (chatRecords != null) {
            saveCache(CACHE_CHAT_PRIVATE, loginUser.getId() + String.valueOf(toId), chatRecords);
        }
        LambdaQueryWrapper<Chat> chatLambdaQueryWrapper = new LambdaQueryWrapper<>();
        chatLambdaQueryWrapper
                .and(privateChat -> privateChat.eq(Chat::getFromId, loginUser.getId()).eq(Chat::getToId,toId)
                        .or()
                        .eq(Chat::getToId,loginUser.getId()).eq(Chat::getFromId,toId)
                ).eq(Chat::getChatType,chatType);
        //双方共有聊天
        List<Chat> list = this.list(chatLambdaQueryWrapper);
        List<ChatMessageVO> chatMessageVOList = list.stream().map(chat -> {
            ChatMessageVO chatMessageVO = chatResult(loginUser.getId(), toId, chat.getText(), chatType, chat.getCreateTime());
            if (chat.getFromId().equals(loginUser.getId())) {
                chatMessageVO.setIsMy(true);
            }
            return chatMessageVO;
        }).collect(Collectors.toList());
        saveCache(CACHE_CHAT_PRIVATE, loginUser.getId() + String.valueOf(toId), chatMessageVOList);
        return chatMessageVOList;
    }

    /**
     * 获取团队聊天
     *
     * @param chatRequest 聊天请求
     * @param chatType    聊天类型
     * @param loginUser   登录用户
     * @return {@link List}<{@link ChatMessageVO}>
     */
    @Override
    public List<ChatMessageVO> getTeamChat(ChatRequest chatRequest, int chatType, User loginUser) {
        Long teamId = chatRequest.getTeamId();
        if (teamId == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"请求有误");
        }
        List<ChatMessageVO> chatRecords = getCache(CACHE_CHAT_TEAM, String.valueOf(teamId));
        if (chatRecords != null) {
            return checkIsMyMessage(loginUser, chatRecords);
        }
        Team team = teamService.getById(teamId);
        LambdaQueryWrapper<Chat> chatLambdaQueryWrapper = new LambdaQueryWrapper<>();
        chatLambdaQueryWrapper.eq(Chat::getChatType, chatType).eq(Chat::getTeamId, teamId);

        List<ChatMessageVO> chatMessageVOS = returnMessage(loginUser, team.getUserId(), chatLambdaQueryWrapper);
        saveCache(CACHE_CHAT_TEAM,String.valueOf(teamId), chatMessageVOS);
        return chatMessageVOS;
    }

    /**
     * 获取 Hall Chat
     * 获取 Hall Chat
     *
     * @param loginUser 登录用户
     * @param chatType  聊天类型
     * @return {@link List}<{@link ChatMessageVO}>
     */
    @Override
    public List<ChatMessageVO> getHallChat(int chatType, User loginUser) {
        List<ChatMessageVO> chatRecords = getCache(CACHE_CHAT_HALL, String.valueOf(loginUser.getId()));
        if (chatRecords != null) {
            List<ChatMessageVO> chatMessageVOS = checkIsMyMessage(loginUser, chatRecords);
            saveCache(CACHE_CHAT_HALL, String.valueOf(loginUser.getId()), chatMessageVOS);
            return chatMessageVOS;
        }
        LambdaQueryWrapper<Chat> chatLambdaQueryWrapper = new LambdaQueryWrapper<>();
        chatLambdaQueryWrapper.eq(Chat::getChatType,chatType);
        List<ChatMessageVO> chatMessageVOList = returnMessage(loginUser, null, chatLambdaQueryWrapper);
        saveCache(CACHE_CHAT_HALL, String.valueOf(loginUser.getId()), chatMessageVOList);
        return chatMessageVOList;
    }

    /**
     * @param key
     * @param id
     */
    @Override
    public void deleteKey(String key, String id) {
        if (key.equals(CACHE_CHAT_HALL)) {
            redisTemplate.delete(key);
        } else {
            redisTemplate.delete(key + id);
        }
    }

    /**
     * 返回消息
     *
     * @param loginUser              登录用户
     * @param userId                 用户 ID
     * @param chatLambdaQueryWrapper 聊天 lambda 查询包装器
     * @return {@link List}<{@link ChatMessageVO}>
     */
    private List<ChatMessageVO> returnMessage(User loginUser, Long userId, LambdaQueryWrapper<Chat> chatLambdaQueryWrapper) {
        List<Chat> chatList = this.list(chatLambdaQueryWrapper);
        return  chatList.stream().map(chat -> {
            ChatMessageVO chatMessageVO = chatResult(chat.getFromId(), chat.getText());
            boolean isCaptain = userId != null && userId.equals(chat.getFromId());
            if (userService.getById(chat.getFromId()).getRole() == ADMIN_ROLE || isCaptain) {
                chatMessageVO.setIsAdmin(true);
            }
            if (chat.getFromId().equals(loginUser.getId())) {
                chatMessageVO.setIsMy(true);
            }
            chatMessageVO.setCreateTime(DateUtil.format(chat.getCreateTime(), "yyyy年MM月dd日 HH:mm:ss"));
            return chatMessageVO;
        }).collect(Collectors.toList());
    }

    /**
     * 检查是我消息
     *
     * @param loginUser   登录用户
     * @param chatRecords 聊天记录
     * @return {@link List}<{@link ChatMessageVO}>
     */
    private List<ChatMessageVO> checkIsMyMessage(User loginUser, List<ChatMessageVO> chatRecords) {
        return chatRecords.stream().peek(chat -> {
            if (chat.getFromUser().getId() != loginUser.getId() && Boolean.TRUE.equals(chat.getIsMy())) {
                chat.setIsMy(false);
            }
            if (chat.getFromUser().getId() == loginUser.getId() && Boolean.TRUE.equals(!chat.getIsMy())) {
                chat.setIsMy(true);
            }
        }).collect(Collectors.toList());
    }

    /**
     * 聊天结果
     *
     * @param toId       到 ID
     * @param text       发短信
     * @param chatType   聊天类型
     * @param createTime 创建时间
     * @param userId     用户 ID
     * @return {@link ChatMessageVO}
     */
    @Override
    public ChatMessageVO chatResult(Long userId, Long toId, String text, int chatType, Date createTime) {
        ChatMessageVO chatMessageVO = new ChatMessageVO();
        User fromUser = userService.getById(userId);
        User toUser = userService.getById(toId);
        WebSocketVO fromWebSocketVO = new WebSocketVO();
        WebSocketVO toWebSocketVO = new WebSocketVO();
        BeanUtils.copyProperties(fromUser, fromWebSocketVO);
        BeanUtils.copyProperties(toUser, toWebSocketVO);
        chatMessageVO.setFromUser(fromWebSocketVO);
        chatMessageVO.setToUser(toWebSocketVO);
        chatMessageVO.setChatType(chatType);
        chatMessageVO.setText(text);
        chatMessageVO.setCreateTime(DateUtil.format(createTime, "yyyy-MM-dd HH:mm:ss"));
        return chatMessageVO;
    }

    /**
     * 聊天结果
     *
     * @param userId 用户 ID
     * @param text   发短信
     * @return {@link ChatMessageVO}
     */
    private ChatMessageVO chatResult(Long userId, String text) {
        ChatMessageVO chatMessageVO = new ChatMessageVO();
        User fromUser = userService.getById(userId);
        WebSocketVO fromWebSocketVO = new WebSocketVO();
        BeanUtils.copyProperties(fromUser, fromWebSocketVO);
        chatMessageVO.setFromUser(fromWebSocketVO);
        return chatMessageVO;
    }

    /**
     * 保存缓存
     *
     * @param redisKey       Redis 密钥
     * @param id             编号
     * @param chatMessageVOS 聊天消息 vos
     */
    private void saveCache(String redisKey, String id, List<ChatMessageVO> chatMessageVOS) {
        try {
            ValueOperations<String, Object> operations = redisTemplate.opsForValue();
            //解决缓存雪崩‘
            int i = RandomUtil.randomInt(MINIMUM_CACHE_RANDOM_TIME, MAXIMUM_CACHE_RANDOM_TIME);
            if (redisKey.equals(CACHE_CHAT_HALL)) {
                operations.set(redisKey, chatMessageVOS, MINIMUM_CACHE_RANDOM_TIME + i / CACHE_TIME_OFFSET, TimeUnit.MINUTES);
            } else {
                operations.set(redisKey + id, chatMessageVOS, MINIMUM_CACHE_RANDOM_TIME + i / CACHE_TIME_OFFSET, TimeUnit.MINUTES);
            }
        } catch (Exception e) {
            log.error("redis set key error");
        }
    }

    /**
     * 获取缓存
     *
     * @param redisKey Redis 密钥
     * @param id       编号
     * @return {@link List}<{@link ChatMessageVO}>
     */
    private List<ChatMessageVO> getCache(String redisKey, String id) {
        ValueOperations<String, Object> operations = redisTemplate.opsForValue();
        List<ChatMessageVO> chatRecords;
        if (redisKey.equals(CACHE_CHAT_HALL)) {
            chatRecords = (List<ChatMessageVO>) operations.get(redisKey);
        } else {
            chatRecords = (List<ChatMessageVO>) operations.get(redisKey + id);
        }
        return chatRecords;
    }
}
