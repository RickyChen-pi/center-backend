package com.ricky.personcenter.service.impl;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.lang.UUID;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.ObjectMetadata;
import com.esotericsoftware.minlog.Log;
import com.ricky.personcenter.common.ErrorCode;
import com.ricky.personcenter.error.BusinessException;
import com.ricky.personcenter.service.FileService;
import com.ricky.personcenter.utils.FileAliOssUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;

/**
 * 文件实现类
 *
 * @author Ricky
 * @date 2024/02/12
 */
@Service
public class FileServiceImpl implements FileService {

    //最大图片大小 2M
    private final long maxFileSize = 2*1024*1024;

    /**
     * 上传文件 到oss
     *
     * @param file 文件
     * @return {@link String}
     */
    @Override
    public String uploadFileAvatar(MultipartFile file) {
        // 工具类获取值
        String endpoint = FileAliOssUtils.END_POINT;
        String accessKeyId = FileAliOssUtils.KTY_ID;
        String accessKeySecret = FileAliOssUtils.KEY_SECRET;
        String bucketName = FileAliOssUtils.BUCKET_NAME;
        try {
            // 创建OSS实例
            OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);
            // 判断文件大小
            long fileSize = file.getSize();
            if (fileSize > maxFileSize) {
                throw new BusinessException(ErrorCode.FORBIDDEN, "文件不能大于2M");
            }

            //获取上传文件的输入流
            InputStream inputStream = file.getInputStream();
            // 获取文件名称
            String filename = file.getOriginalFilename();

            //使用UUID进行重命名
            filename = UUID.randomUUID().toString().replaceAll("-", "")+filename;
            //将文件按照日期进行分类
            String datePath = new DateTime().toString("yyyy/MM/dd");
            filename = datePath + "/" + filename;


            // 调用oss实例中的方法实现上传
            // 参数1： Bucket名称
            // 参数2： 上传到oss文件路径和文件名称 /aa/bb/1.jpg
            // 参数3： 上传文件的输入流
            ossClient.putObject(bucketName, filename, inputStream);

            // 关闭OSSClient。
            ossClient.shutdown();

            // 把上传后文件路径返回，需要把上传到阿里云oss路径手动拼接出来
            return "https://" + bucketName + "." + endpoint + "/" + filename;
        } catch (IOException e) {
            Log.error("fileService error", e);
            return null;
        }

    }

}
