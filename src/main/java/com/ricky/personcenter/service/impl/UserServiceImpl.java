package com.ricky.personcenter.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ricky.personcenter.common.ErrorCode;
import com.ricky.personcenter.error.BusinessException;
import com.ricky.personcenter.mapper.UserMapper;
import com.ricky.personcenter.model.entity.Follow;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.request.UpdatePasswordRequest;
import com.ricky.personcenter.model.request.UserRegisterRequest;
import com.ricky.personcenter.model.request.UserUpdateRequest;
import com.ricky.personcenter.model.vo.UserVO;
import com.ricky.personcenter.service.CacheService;
import com.ricky.personcenter.service.FollowService;
import com.ricky.personcenter.service.UserService;
import com.ricky.personcenter.utils.AlgorithmUtils;
import javafx.util.Pair;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.BeanUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.ricky.personcenter.contant.UserConstant.ADMIN_ROLE;
import static com.ricky.personcenter.contant.UserConstant.USER_LOGIN_STATE;

/**
 * @author Ricky
 * &#064;description  针对表【user】的数据库操作Service实现
 * &#064;createDate  2023-12-21 20:54:16
 */
@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
        implements UserService {

    /**
     * 盐值
     */
    private static final String SALT = "chenpi";
    @Resource
    private UserMapper userMapper;
    @Resource
    private FollowService followService;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private CacheService cacheService;
    @Resource
    private RedissonClient redissonClient;

    /**
     * 用户注册
     *
     * @param userAccount   用户帐号
     * @param userPassword  用户密码
     * @param checkPassword 检查密码
     */
    @Override
    public void userRegister(String userAccount, String userPassword, String checkPassword) {
        //校验
        if (StringUtils.isAnyBlank(userAccount, userPassword, checkPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "参数为空");
        }
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户账号过短");
        }
        if (userPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户密码过短");
        }
        //账号不能存在特殊字符
        String validPattern = "^[a-zA-Z0-9 ]+$";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if (!matcher.find()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号存在特殊字符");
        }
        //密码和校验密码相同
        if (!userPassword.equals(checkPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "两次密码不一致");
        }
        //账号不能重复
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userAccount", userAccount);
        long count = userMapper.selectCount(queryWrapper);
        if (count > 0) {
            throw new BusinessException(ErrorCode.NULL_RESPONSE, "账号重复");
        }
        //2、密码加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());

        RLock lock = redissonClient.getLock("personcenter:register");

        boolean isLock = false;
        try {
            isLock = lock.tryLock(5,10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            throw new BusinessException(ErrorCode.NULL_RESPONSE,"注册失败");
        }
        if (!isLock) {
            log.error("无法获取注册分布式锁");
            throw new BusinessException(ErrorCode.NULL_RESPONSE, "注册失败");
        }

        try {
            // 解决数据库和Redis缓存的一致性问题··面试解决的问题
            redisTemplate.delete(Objects.requireNonNull(redisTemplate.keys("personcenter:user:recommend:" + "*:*:*")));

            // 插入数据
            User user = new User();
            user.setUserAccount(userAccount);
            user.setUserPassword(encryptPassword);
            user.setUsername(userAccount);
            user.setTags("[\"游客\"]");
            user.setGender(2);
            user.setStatus(0);
            user.setIsDelete(0);
            user.setRole(0);

            boolean saveResult = this.save(user);
            if (!saveResult) {
                log.error("注册失败，保存用户信息时出错");
                throw new BusinessException(ErrorCode.NULL_RESPONSE, "注册失败");
            }

            // 再次删除缓存
            cacheService.deleteRecommendCache(userAccount);
        } finally {
            if (lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }

    /**
     * 做登录
     *
     * @param userAccount  用户帐户
     * @param userPassword 用户密码
     * @return {@link User}
     */
    @Override
    public User doLogin(String userAccount, String userPassword, HttpServletRequest request) {
        if (StringUtils.isAnyBlank(userAccount, userPassword)) {
            throw new BusinessException(ErrorCode.NULL_ERROR, "密码不能为空");
        }
        if (userAccount.length() < 4) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户账号过短");
        }
        if (userPassword.length() < 8) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "密码过短");
        }
        //账号不能存在特殊字符
        String validPattern = "^[a-zA-Z0-9 ]+$";
        Matcher matcher = Pattern.compile(validPattern).matcher(userAccount);
        if (!matcher.find()) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "账号存在特殊字符");
        }

        //2、加密
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());
        //检查用户是否存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("userAccount", userAccount);
        queryWrapper.eq("userPassword", encryptPassword);
        User user = userMapper.selectOne(queryWrapper);
        if (user == null) {
            log.info("user login failed, userAccount cannot be matched");
            throw new BusinessException(ErrorCode.NULL_RESPONSE, "密码错误");
        }
        //3、用户脱敏
        User safetyUser = getSafetyUser(user);

        //4、记录用户的登录状态
        request.getSession().setAttribute(USER_LOGIN_STATE, safetyUser);

        return safetyUser;
    }

    /**
     * 用户注销
     *
     * @param request 请求
     */
    @Override
    public int userLoginOut(HttpServletRequest request) {
        request.getSession().removeAttribute(USER_LOGIN_STATE);
        return 1;
    }

    /**
     * 按标签搜索用户(内存过滤版)
     *
     * @param tagNameList 标签列表
     * @return int
     */
    @Override
    public Page<User> searchUserByTags(List<String> tagNameList, long currentPage) {
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        for (String tagName : tagNameList) {
            userLambdaQueryWrapper.or().like(StringUtils.isNotEmpty(tagName), User::getTags, tagName);
        }
        return page(new Page<>(currentPage, 8), userLambdaQueryWrapper);
    }

    /**
     * 按 SQL 按标签搜索用户
     *
     * @param tagNameList 标签名称列表
     * @return {@link List}<{@link User}>
     */
    @Deprecated
    private List<User> searchUserByTagsBySQL(List<String> tagNameList) {
        if (CollectionUtils.isEmpty(tagNameList)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }

        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        for (String tagName : tagNameList) {
            queryWrapper = queryWrapper.like("tags", tagName);
        }

        List<User> userList = userMapper.selectList(queryWrapper);

        return userList.stream().map(this::getSafetyUser).collect(Collectors.toList());
    }

    /**
     * 更新用户信息
     *
     * @param updateRequest 更新请求
     * @param loginUser     登录用户
     * @return {@link Integer}
     */
    @Override
    public Integer updateUser(UserUpdateRequest updateRequest, User loginUser) {
        long userId = updateRequest.getId();
        if (userId <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        //如果是管理员，允许更新所有用户信息
        //如果不是管理员，只允许跟新自己的信息
        if (userId != loginUser.getId()) {
            throw new BusinessException(ErrorCode.NO_AUTH);
        }
        User oldUser = userMapper.selectById(userId);
        if (oldUser == null) {
            throw new BusinessException(ErrorCode.NULL_ERROR);
        }

        RLock lock = redissonClient.getLock("personcenter:updateUser");
        boolean isLock = false;
        try {
            isLock = lock.tryLock(5, 10, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            log.error("获取锁失败");
            throw new BusinessException(ErrorCode.NULL_RESPONSE,"修改信息失败");
        }
        if (!isLock) {
            log.error("获取锁失败");
            throw new BusinessException(ErrorCode.NULL_RESPONSE,"修改信息失败");
        }

        try {
            redisTemplate.delete(Objects.requireNonNull(redisTemplate.keys("personcenter:user:recommend:" + "*:*:*")));
            User user = new User();
            BeanUtils.copyProperties(updateRequest, user);
            user.setId(userId);
            int i = userMapper.updateById(user);
            if (i <= 0) {
                throw new BusinessException(ErrorCode.NULL_RESPONSE,"修改信息失败");
            }
            cacheService.deleteRecommendCache(user.getUserAccount());
            return i;
        } finally {
            if (lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }

    /**
     * 获取已登录用户信息
     *
     * @param request 请求
     * @return {@link User}
     */
    @Override
    public User getLoginUser(HttpServletRequest request) {
        if (request == null) {
            return null;
        }
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        if (userObj == null) {
            //throw new BusinessException(ErrorCode.NO_AUTH);
            return null;
        }
        return (User) userObj;
    }

    /**
     * 判断是否为管理员
     *
     * @param request 请求
     * @return boolean
     */
    @Override
    public boolean isAdmin(HttpServletRequest request) {
        Object userObj = request.getSession().getAttribute(USER_LOGIN_STATE);
        User user = (User) userObj;
        return user != null && user.getRole() == ADMIN_ROLE;
    }

    @Override
    public boolean isAdmin(User loginUser) {
        return loginUser != null && loginUser.getRole() == ADMIN_ROLE;
    }

    /**
     * 匹配用户
     *
     * @param num       数字
     * @param loginUser 登录用户
     * @return {@link List}<{@link UserVO}>
     */
    @Override
    public List<User> matchUsers(long num, User loginUser) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("id", "tags");
        queryWrapper.isNotNull("tags");
        List<User> userList = this.list(queryWrapper);
        String tags = loginUser.getTags();
        Gson gson = new Gson();
        List<String> tagList = gson.fromJson(tags, new TypeToken<List<String>>() {
        }.getType());
        // 用户列表的下标 => 相似度
        List<Pair<User, Long>> list = new ArrayList<>();
        // 依次计算所有用户和当前用户的相似度
        for (int i = 0; i < userList.size(); i++) {
            User user = userList.get(i);
            String userTags = user.getTags();
            // 无标签或者为当前用户自己
            if (StringUtils.isBlank(userTags) || user.getId().equals(loginUser.getId())) {
                continue;
            }
            List<String> userTagList = gson.fromJson(userTags, new TypeToken<List<String>>() {
            }.getType());
            // 计算分数
            long distance = AlgorithmUtils.minDistance(tagList, userTagList);
            list.add(new Pair<>(user, distance));
        }
        // 按编辑距离由小到大排序
        List<Pair<User, Long>> topUserPairList = list.stream()
                .sorted((a, b) -> (int) (a.getValue() - b.getValue()))
                .limit(num)
                .collect(Collectors.toList());
        // 原本顺序的 userId 列表
        List<Long> userIdList = topUserPairList.stream().map(pair -> pair.getKey().getId()).collect(Collectors.toList());
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.in("id", userIdList);
        // 1, 3, 2
        // User1、User2、User3
        // 1 => User1, 2 => User2, 3 => User3
        Map<Long, List<User>> userIdUserListMap = this.list(userQueryWrapper)
                .stream()
                .map(user -> getSafetyUser(user))
                .collect(Collectors.groupingBy(User::getId));
        List<User> finalUserList = new ArrayList<>();
        for (Long userId : userIdList) {
            finalUserList.add(userIdUserListMap.get(userId).get(0));
        }
        return finalUserList;
    }

    /**
     * 按 ID 获取用户
     *
     * @param userId      用户 ID
     * @param loginUserId 登录用户 ID
     * @return {@link UserVO}
     */
    @Override
    public UserVO getUserById(Long userId, Long loginUserId) {
        User user = this.getById(userId);
        UserVO userVO = new UserVO();
        BeanUtils.copyProperties(user, userVO);
        LambdaQueryWrapper<Follow> followLambdaQueryWrapper = new LambdaQueryWrapper<>();
        //先查询follow表中登录用户的所有数据，然后再从中查找目标userId的值，看是否已经有关注
        followLambdaQueryWrapper
                .eq(Follow::getUserId, loginUserId)
                .eq(Follow::getFollowUserId, userId);
        long count = followService.count(followLambdaQueryWrapper);
        userVO.setIsFollow(count > 0);
        return userVO;
    }

    /**
     * 更新密码
     *
     * @param passwordRequest 密码请求
     * @param loginUser       登录用户
     * @return {@link Integer}
     */
    @Override
    public Integer updatePassword(UpdatePasswordRequest passwordRequest, User loginUser) {
        String newPassword = passwordRequest.getNewPassword();
        String originPassword = passwordRequest.getOriginPassword();
        String confirmPassword = passwordRequest.getConfirmPassword();
        if (!Objects.equals(newPassword, confirmPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "两次新密码不一致");
        }
        if (Objects.equals(newPassword, originPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "新密码和原密码不能一样");
        }

        //密码加密
        String newEcryptPwd = DigestUtils.md5DigestAsHex((SALT + newPassword).getBytes());
        String oriEcryptPwd = DigestUtils.md5DigestAsHex((SALT + originPassword).getBytes());

        User user = this.getById(loginUser);
        String passwordDb = user.getUserPassword();
        if (Objects.equals(passwordDb, newEcryptPwd)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "新密码和原密码不能一致");
        }
        if (!Objects.equals(passwordDb, oriEcryptPwd)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "原密码错误");
        }

        user.setUserPassword(newEcryptPwd);
        return userMapper.updateById(user);
    }

    /**
     * 获取当前用户标签
     *
     * @param id 编号
     * @return {@link List}<{@link String}>
     */
    @Override
    public List<String> getUserTags(Long id) {
        User user = this.getById(id);
        String userTags = user.getTags();
        Gson gson = new Gson();
        return gson.fromJson(userTags, new TypeToken<List<String>>() {
        }.getType());
    }

    /**
     * 更新标签
     *
     * @param tags        标签
     * @param loginUserId 登录用户 ID
     */
    @SneakyThrows(Exception.class)
    @Override
    public void updateTags(List<String> tags, Long loginUserId) {
        User user = new User();
        Gson gson = new Gson();
        String tagsJson = gson.toJson(tags);
        user.setId(loginUserId);
        user.setTags(tagsJson);
        User userDb = this.getById(loginUserId);

        RLock lock = redissonClient.getLock("personcenter:updateTags");
        boolean isLock = false;
        isLock = lock.tryLock(5,10,TimeUnit.SECONDS);
        if (!isLock) {
            log.error("获取更新锁失败");
            throw new BusinessException(ErrorCode.NULL_RESPONSE,"标签更新失败");
        }

        try {
            redisTemplate.delete(Objects.requireNonNull(redisTemplate.keys("personcenter:user:recommend:" + "*:*:*")));
            this.updateById(user);
            cacheService.deleteRecommendCache(userDb.getUserAccount());
        } finally {
            if (lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }
    }

    /**
     * 找回密码
     *
     * @param registerRequest 注册请求
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void forget(UserRegisterRequest registerRequest) {
        String userAccount = registerRequest.getUserAccount();
        String userPassword = registerRequest.getUserPassword();
        String checkPassword = registerRequest.getCheckPassword();

        if (!checkPassword.equals(userPassword)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "两次密码不一致");
        }
        LambdaQueryWrapper<User> userLambdaQueryWrapper = new LambdaQueryWrapper<>();
        userLambdaQueryWrapper.eq(User::getUserAccount, userAccount);
        User user = this.getOne(userLambdaQueryWrapper);
        if (user == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "用户不存在");
        }
        String encryptPassword = DigestUtils.md5DigestAsHex((SALT + userPassword).getBytes());
        String userPasswordDb = user.getUserPassword();
        if (encryptPassword.equals(userPasswordDb)) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "不能和原密码一致");
        }

        User updateUser = new User();
        updateUser.setId(user.getId());
        updateUser.setUserPassword(encryptPassword);
        boolean updateResult = this.updateById(updateUser);
        if (!updateResult) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "密码找回失败，请重新操作");
        }
    }


    /**
     * 用户脱敏
     *
     * @param originUser 源用户
     * @return {@link User}
     */
    @Override
    public User getSafetyUser(User originUser) {
        if (originUser == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "传入数据为空");
        }
        User safetyUser = new User();
        safetyUser.setId(originUser.getId());
        safetyUser.setUsername(originUser.getUsername());
        safetyUser.setUserAccount(originUser.getUserAccount());
        safetyUser.setAvatarUrl(originUser.getAvatarUrl());
        safetyUser.setGender(originUser.getGender());
        safetyUser.setPhone(originUser.getPhone());
        safetyUser.setEmail(originUser.getEmail());
        safetyUser.setRole(originUser.getRole());
        safetyUser.setStatus(originUser.getStatus());
        safetyUser.setCreateTime(originUser.getCreateTime());
        safetyUser.setTags(originUser.getTags());
        safetyUser.setProfile(originUser.getProfile());
        return safetyUser;
    }

}




