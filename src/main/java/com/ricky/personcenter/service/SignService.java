package com.ricky.personcenter.service;

import com.ricky.personcenter.model.entity.Sign;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Ricky
* @description 针对表【sign】的数据库操作Service
* @createDate 2024-02-07 16:45:00
*/
public interface SignService extends IService<Sign> {

}
