package com.ricky.personcenter.service;

import com.ricky.personcenter.model.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author Ricky
* @description 针对表【message】的数据库操作Service
* @createDate 2024-02-07 16:45:00
*/
public interface MessageService extends IService<Message> {

    Boolean hasNewMessage(Long userId);

    /**
     * 获取新消息数量
     *
     * @param loginId 登录 ID
     * @return long
     */
    long getMessageNum(Long loginId);
}
