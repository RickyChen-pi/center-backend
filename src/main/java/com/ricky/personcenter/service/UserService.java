package com.ricky.personcenter.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.request.UpdatePasswordRequest;
import com.ricky.personcenter.model.request.UserRegisterRequest;
import com.ricky.personcenter.model.request.UserUpdateRequest;
import com.ricky.personcenter.model.vo.UserVO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
* @author Ricky
* @description 针对表【user】的数据库操作Service
* @createDate 2023-12-21 20:54:16
*/
public interface UserService extends IService<User> {


    /**
     * 用户注册
     *
     * @param userAccount   用户账户
     * @param userPassword  用户密码
     * @param checkPassword 检查密码
     */
    void userRegister(String userAccount, String userPassword, String checkPassword);

    /**
     * 做登录
     *
     * @param userAccount  用户帐户
     * @param userPassword 用户密码
     * @return {@link User}
     */
    User doLogin(String userAccount, String userPassword, HttpServletRequest request);

    /**
     * 用户注销
     *
     * @param request 请求
     */
    int userLoginOut(HttpServletRequest request);

    /**
     * 按标签搜索用户
     *
     * @param tagNameList 标签列表
     * @return int
     */
    Page<User> searchUserByTags(List<String> tagNameList, long currentPage);

    User getSafetyUser(User originUser);

    /**
     * 更新用户信息
     *
     * @param user 用户
     * @return {@link Integer}
     */
    Integer updateUser(UserUpdateRequest updateRequest, User loginUser);

    /**
     * 获取登录用户
     *
     * @param request 请求
     * @return {@link User}
     */
    User getLoginUser(HttpServletRequest request);

    /**
     * 是否为管理员
     *
     * @param request 请求
     * @return boolean
     */
     boolean isAdmin(HttpServletRequest request);
    boolean isAdmin(User loginUser);


    /**
     * 获取最匹配的用户
     *
     * @param num  数字
     * @param loginUser 用户
     * @return {@link List}<{@link UserVO}>
     */
    List<User> matchUsers(long num, User loginUser);

    /**
     * 按 ID 获取用户
     *
     * @param id  编号
     * @param id1 id1
     * @return {@link UserVO}
     */
    UserVO getUserById(Long userId, Long loginUserId);

    /**
     * 更新密码
     *
     * @param passwordRequest 密码请求
     * @param loginUser       登录用户
     * @return {@link Integer}
     */
    Integer updatePassword(UpdatePasswordRequest passwordRequest, User loginUser);

    /**
     * 获取当前用户标签
     *
     * @param id 编号
     * @return {@link List}<{@link String}>
     */
    List<String> getUserTags(Long id);

    /**
     * 更新标签
     *
     * @param tags 标签
     * @param id   编号
     */
    void updateTags(List<String> tags, Long loginUserId);

    /**
     * 忘记
     *
     * @param registerRequest 注册请求
     */
    void forget(UserRegisterRequest registerRequest);
}
