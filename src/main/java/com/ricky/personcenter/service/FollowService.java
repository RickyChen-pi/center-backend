package com.ricky.personcenter.service;

import com.ricky.personcenter.model.entity.Follow;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.vo.UserVO;

import java.util.List;

/**
* @author Ricky
* @description 针对表【follow】的数据库操作Service
* @createDate 2024-02-07 16:45:00
*/
public interface FollowService extends IService<Follow> {

    void followUser(Long userId, Long loginUserId);

    /**
     * 列出我关注的用户
     *
     * @param id 编号
     * @return {@link List}<{@link UserVO}>
     */
    List<UserVO> listMyFollow(Long loginUserId);

    /**
     * 获取粉丝名单
     *
     * @param id 编号
     * @return {@link List}<{@link UserVO}>
     */
    List<UserVO> listFans(Long loginUserId);

    /**
     * 获取用户关注信息
     *
     * @param user        用户
     * @param loginUserId 登录用户 ID
     * @return {@link UserVO}
     */
    UserVO getUserFollowInfo(User user, Long loginUserId);
}
