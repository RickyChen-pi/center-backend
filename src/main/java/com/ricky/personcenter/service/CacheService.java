package com.ricky.personcenter.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author Ricky
 * @date 2024/02/23
 */
@Component
@Slf4j
public class CacheService {

    @Resource
    private RedisTemplate<String, String> redisTemplate;

    @Async
    public void deleteRecommendCache(String userAccount) {
        //延迟一段时间，1秒
        try {
            TimeUnit.SECONDS.sleep(1);
            //再次删除缓存中不再有过期的数据
            redisTemplate.delete(Objects.requireNonNull(redisTemplate.keys("personcenter:user:recommend:" + "*:*:*")));
        } catch (InterruptedException e) {
            log.error("延迟删除缓存时发生中断", e);
        }
    }

}