package com.ricky.personcenter.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * 文件服务
 *
 * @author Ricky
 * @date 2024/02/12
 */
public interface FileService {
    /**
     * 上传头像到OSS
     *
     * @param file 文件
     * @return {@link String}
     */
    String uploadFileAvatar(MultipartFile file);
}