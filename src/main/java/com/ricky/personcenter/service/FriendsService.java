package com.ricky.personcenter.service;

import com.ricky.personcenter.model.entity.Friends;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.request.FriendAddRequest;
import com.ricky.personcenter.model.vo.FriendsRecordVO;

import java.util.List;

/**
* @author Ricky
* @description 针对表【friends】的数据库操作Service
* @createDate 2024-02-07 16:45:00
*/
public interface FriendsService extends IService<Friends> {

    /**
     * 添加好友
     *
     * @param friendAddRequest 好友添加请求
     * @param loginUser        登录用户
     * @return boolean
     */
    boolean addFriends(FriendAddRequest friendAddRequest, User loginUser);

    /**
     * 查询添加好友的记录
     *
     * @param loginUser 登录用户
     * @return {@link List}<{@link FriendsRecordVO}>
     */
    List<FriendsRecordVO> getRecords(User loginUser);

    /**
     * 同意申请
     *
     * @param loginUser 登录用户
     * @param fromId    从 ID
     * @return boolean
     */
    boolean agreeToApply(User loginUser, Long fromId);

    /**
     * 拒绝申请
     *
     * @param loginUser 登录用户
     * @param id        编号
     * @return boolean
     */
    boolean canceledApply(User loginUser, Long id);

}
