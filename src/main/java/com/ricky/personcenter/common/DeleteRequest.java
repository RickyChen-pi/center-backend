package com.ricky.personcenter.common;

import lombok.Data;

import java.io.Serializable;

/**
 * 通用的删除请求参数
 *
 * @author Ricky
 * @date 2024/01/19
 */
@Data
public class DeleteRequest implements Serializable {

    private static final long serialVersionUID = 2433350837176226181L;

    private long id;
}
