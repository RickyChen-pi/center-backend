package com.ricky.personcenter.controller;


import com.ricky.personcenter.common.BaseResponse;
import com.ricky.personcenter.common.ErrorCode;
import com.ricky.personcenter.common.ResultUtils;
import com.ricky.personcenter.error.BusinessException;
import com.ricky.personcenter.manager.RedisLimiterManager;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.vo.UserVO;
import com.ricky.personcenter.service.FollowService;
import com.ricky.personcenter.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 关注控制器
 *
 * @author Ricky
 * @date 2024/02/07
 */
@Slf4j
@RestController
@RequestMapping("/follow")
public class FollowController {

    @Resource
    private FollowService followService;

    @Resource
    private UserService userService;

    @Resource
    private RedisLimiterManager limiterManager;

    @PostMapping("/{id}")
    public BaseResponse<String> followUser(@PathVariable Long id, HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN,"未登录");
        }
        //限流--每个用户一秒最多只能访问两次
        boolean rateLimit = limiterManager.doRateLimit(loginUser.getId().toString());
        if (!rateLimit) {
            throw new BusinessException(ErrorCode.TOO_MANY_REQUEST, "请求过于频繁");
        }

        followService.followUser(id, loginUser.getId());
        return ResultUtils.success("ok");
    }

    /**
     * 获取我关注的用户
     *
     * @param request 请求
     * @return {@link BaseResponse}<{@link List}<{@link UserVO}>>
     */
    @GetMapping("/my")
    public BaseResponse<List<UserVO>> listMyFollow(HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        List<UserVO> userVOList = followService.listMyFollow(loginUser.getId());
        return ResultUtils.success(userVOList);
    }

    /**
     * 获取粉丝名单
     *
     * @param request 请求
     * @return {@link BaseResponse}<{@link List}<{@link UserVO}>>
     */
    @GetMapping("/fans")
    public BaseResponse<List<UserVO>> listFans(HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        List<UserVO> userVOList = followService.listFans(loginUser.getId());
        return ResultUtils.success(userVOList);
    }

}
