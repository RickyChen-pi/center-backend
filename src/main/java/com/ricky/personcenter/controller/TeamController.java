package com.ricky.personcenter.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ricky.personcenter.common.BaseResponse;
import com.ricky.personcenter.common.DeleteRequest;
import com.ricky.personcenter.common.ErrorCode;
import com.ricky.personcenter.common.ResultUtils;
import com.ricky.personcenter.error.BusinessException;
import com.ricky.personcenter.manager.RedisLimiterManager;
import com.ricky.personcenter.model.dto.TeamqueryDTO;
import com.ricky.personcenter.model.entity.Team;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.entity.UserTeam;
import com.ricky.personcenter.model.request.*;
import com.ricky.personcenter.model.vo.TeamUserVO;
import com.ricky.personcenter.model.vo.TeamVO;
import com.ricky.personcenter.model.vo.UserVO;
import com.ricky.personcenter.service.TeamService;
import com.ricky.personcenter.service.UserService;
import com.ricky.personcenter.service.UserTeamService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 组队接口
 *
 * @author Ricky
 * @date 2024/01/15
 */
@RestController
@Slf4j
@RequestMapping("/team")
public class TeamController {

    @Resource
    private UserService userService;
    @Resource
    private TeamService teamService;
    @Resource
    private UserTeamService userTeamService;
    @Resource
    private RedisLimiterManager redisLimiterManager;

    /**
     * 创建队伍
     *
     * @param teamAddRequest 团队
     * @return {@link BaseResponse}<{@link Long}>
     */
    @PostMapping("/add")
    public BaseResponse<Long> addTeam(@RequestBody TeamAddRequest teamAddRequest, HttpServletRequest request){
        if (teamAddRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        boolean rateLimit = redisLimiterManager.doRateLimit(loginUser.getId().toString());
        if (!rateLimit) {
            throw new BusinessException(ErrorCode.TOO_MANY_REQUEST,"操作太频繁，请重试");
        }
        Team team = new Team();
        BeanUtils.copyProperties(teamAddRequest,team);
        long teamId = teamService.addTeam(team, loginUser);
        return ResultUtils.success(teamId);
    }

    /**
     * 删除团队
     *
     * @return {@link BaseResponse}<{@link Boolean}>
     */
    @PostMapping("/delete")
    public BaseResponse<Boolean> deleteTeam(@RequestBody DeleteRequest deleteRequest, HttpServletRequest request){
        if (deleteRequest.getId() <= 0 || deleteRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        long teamId = deleteRequest.getId();
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        boolean isAdmin = userService.isAdmin(loginUser);
        boolean result = teamService.deleteTeam(teamId, loginUser, isAdmin);

        if (!result){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR,"删除失败");
        }
        return ResultUtils.success(true);
    }

    /**
     * 修改队伍信息
     *
     * @param teamUpdateRequest 团队更新请求
     * @param request           请求
     * @return {@link BaseResponse}<{@link Boolean}>
     */
    @PostMapping("/update")
    public BaseResponse<Boolean> updateTeam(@RequestBody TeamUpdateRequest teamUpdateRequest, HttpServletRequest request){
        if (teamUpdateRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null){
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        boolean result = teamService.updateTeam(teamUpdateRequest, loginUser);
        if (!result){
            throw new BusinessException(ErrorCode.SYSTEM_ERROR,"修改失败");
        }
        return ResultUtils.success(true);
    }

    /**
     * 按 ID 获取队伍
     *
     * @param id 编号
     * @return {@link BaseResponse}<{@link Team}>
     */
    @GetMapping ("/get")
    public BaseResponse<TeamVO> getTeamById( long id){
        if (id <= 0){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team team = teamService.getById(id);
        if (team == null){
            throw new BusinessException(ErrorCode.NULL_ERROR);
        }
        TeamVO teamVO = new TeamVO();
        BeanUtils.copyProperties(team, teamVO);
        return ResultUtils.success(teamVO);
    }

    /**
     * 获取队伍列表
     *
     * @param request          请求
     * @param currentPage      当前页面
     * @param teamQueryRequest 团队查询请求
     * @return {@link BaseResponse}<{@link Page}<{@link TeamVO}>>
     */
    @GetMapping("/list")
    public BaseResponse<Page<TeamVO>> listTeams(long currentPage, TeamQueryRequest teamQueryRequest, HttpServletRequest request) {
        if (teamQueryRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR,"参数不能为空");
        }
        User loginUser = userService.getLoginUser(request);
        Page<TeamVO> teamVoPage = teamService.listTeams(currentPage, teamQueryRequest, userService.isAdmin(loginUser));
        Page<TeamVO> finalPage = teamService.getJoinedUserAvatar(teamVoPage);
        return getUserJoinedList(loginUser,finalPage);
    }

    /**
     * 获取已加入队伍的用户
     *
     * @param loginUser 登录用户
     * @return {@link BaseResponse}<{@link List}<{@link TeamVO}>>
     */
    private BaseResponse<Page<TeamVO>> getUserJoinedList(User loginUser, Page<TeamVO> teamPage) {
        try {
            List<TeamVO> teamList = teamPage.getRecords();
            List<Long> teamIdList = teamList.stream().map(TeamVO::getId).collect(Collectors.toList());
            //判断当前用户已加入的队伍
            LambdaQueryWrapper<UserTeam> userTeamLambdaQueryWrapper = new LambdaQueryWrapper<>();
            userTeamLambdaQueryWrapper.eq(UserTeam::getUserId, loginUser.getId()).in(UserTeam::getTeamId, teamIdList);
            //用户已加入的队伍
            List<UserTeam> userTeamList = userTeamService.list(userTeamLambdaQueryWrapper);
            Set<Long> joinedTeamIdList = userTeamList.stream().map(UserTeam::getTeamId).collect(Collectors.toSet());
            teamList.forEach(team -> team.setHasJoin(joinedTeamIdList.contains(team.getId())));
            teamPage.setRecords(teamList);
        } catch (Exception ignored) {
        }
        return ResultUtils.success(teamPage);
    }

    /**
     * 按页面列出队伍信息
     *
     * @param teamqueryDTO TeamQuery DTO
     * @return {@link BaseResponse}<{@link Page}<{@link Team}>>
     */
    @GetMapping("/list/page")
    public BaseResponse<Page<Team>> listTeamsByPage(TeamqueryDTO teamqueryDTO){
        if (teamqueryDTO == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        Team team = new Team();
        BeanUtils.copyProperties(teamqueryDTO, team);
        Page<Team> page = new Page<>(teamqueryDTO.getPageNum(), teamqueryDTO.getPageSize());
        QueryWrapper<Team> queryWrapper = new QueryWrapper<>(team);
        Page<Team> resultPage = teamService.page(page, queryWrapper);
        return ResultUtils.success(resultPage);
    }

    /**
     * 退出队伍
     *
     * @param teamquitRequest Teamquit 请求
     * @return {@link BaseResponse}<{@link Boolean}>
     */
    @PutMapping("/quit")
    public BaseResponse<Boolean> quitTeam(@RequestBody TeamquitRequest teamquitRequest, HttpServletRequest request){
        if (teamquitRequest == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        boolean result = teamService.quitTeam(teamquitRequest,loginUser);
        return ResultUtils.success(result);
    }

    /**
     * 加入队伍
     *
     * @param teamJoinRequest 团队加入请求
     * @param request         请求
     * @return {@link BaseResponse}<{@link Boolean}>
     */
    @PostMapping("/join")
    public BaseResponse<Boolean> joinTeam(@RequestBody TeamJoinRequest teamJoinRequest, HttpServletRequest request){
        if (teamJoinRequest == null){
            throw new BusinessException(ErrorCode.NULL_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        boolean result = teamService.joinTeam(teamJoinRequest, loginUser);
        return ResultUtils.success(result);
    }

    /**
     * 列出我创建团队
     *
     * @param request     请求
     * @param currentPage 当前页面
     * @param teamQuery   团队查询
     * @return {@link BaseResponse}<{@link Page}<{@link TeamUserVO}>>
     */
    @GetMapping("/list/my/create")
    public BaseResponse<Page<TeamVO>> listMyCreateTeams(long currentPage,
                                                        TeamQueryRequest teamQuery,
                                                        HttpServletRequest request){
        if (teamQuery == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null){
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        teamQuery.setUserId(loginUser.getId());
        Page<TeamVO> teamVOPage = teamService.listMyCreate(currentPage, loginUser.getId());
        return getUserJoinedList(loginUser,teamVOPage);
    }

    /**
     * 获取我加入的队伍
     *
     * @param teamqueryDTO TeamQuery DTO
     * @param request      请求
     * @return {@link BaseResponse}<{@link List}<{@link TeamUserVO}>>
     */
    @GetMapping("/list/my/join")
    public BaseResponse<Page<TeamVO>> listMyJoinTeams(long currentPage,
                                                          TeamQueryRequest teamQuery,
                                                          HttpServletRequest request){
        if (teamQuery == null){
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null){
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }

        QueryWrapper<UserTeam> userTeamQueryWrapper = new QueryWrapper<>();
        userTeamQueryWrapper.eq("userId", loginUser.getId());
        //查询出我已加入的队伍
        List<UserTeam> userTeamList = userTeamService.list(userTeamQueryWrapper);
        //取出不重复的队伍id
        Map<Long, List<UserTeam>> listMap = userTeamList.stream()
                .collect(Collectors.groupingBy(UserTeam::getTeamId));
        List<Long> idList = new ArrayList<>(listMap.keySet());
        if (idList.isEmpty()) {
            return ResultUtils.success(new Page<>());
        }
        teamQuery.setIdList(idList);

        Page<TeamVO> teamVOPage = teamService.listMyJoin(currentPage,teamQuery);

        return getUserJoinedList(loginUser,teamVOPage);
    }


    @GetMapping("/list/my/join/all")
    public BaseResponse<List<TeamVO>> listAllMyJoinTeams(HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        List<TeamVO> teamVOList = teamService.listAllMyJoin(loginUser.getId());
        return ResultUtils.success(teamVOList);
    }

    /**
     * 按 ID 获取团队
     *
     * @param id      编号
     * @param request 请求
     * @return {@link BaseResponse}<{@link TeamVO}>
     */
    @GetMapping("/{id}")
    public BaseResponse<TeamVO> getTeamById(@PathVariable Long id, HttpServletRequest request) {
        if (id == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        return ResultUtils.success(teamService.getTeam(id,loginUser.getId()));
    }

    /**
     * 按 ID 获取团队成员
     *
     * @param id      编号
     * @param request 请求
     * @return {@link BaseResponse}<{@link List}<{@link UserVO}>>
     */
    @GetMapping("/member/{id}")
    public BaseResponse<List<UserVO>> getTeamMemberById(@PathVariable Long id, HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }

        if (id == null || id < 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        List<UserVO> teamMember = teamService.getTeamMember(id, loginUser.getId());
        return ResultUtils.success(teamMember);
    }

    /**
     * 更改封面图片
     *
     * @param teamCoverUpdateRequest 团队封面更新请求
     * @param request                请求
     * @return {@link BaseResponse}<{@link String}>
     */
    @PutMapping("/cover")
    public BaseResponse<String> changeCoverImage(TeamCoverUpdateRequest teamCoverUpdateRequest, HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        boolean admin = userService.isAdmin(loginUser);
        teamService.changeCoverImage(teamCoverUpdateRequest, loginUser.getId(), admin);
        return ResultUtils.success("ok");
    }

    /**
     * 踢出去
     *
     * @param teamKickOutRequest 团队踢出请求
     * @param request            请求
     * @return {@link BaseResponse}<{@link String}>
     */
    @PostMapping("/kick")
    public BaseResponse<String> kickOut(@RequestBody TeamKickOutRequest teamKickOutRequest, HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        Long teamId = teamKickOutRequest.getTeamId();
        Long userId = teamKickOutRequest.getUserId();
        if (teamId == null || teamId <= 0 || userId == null || userId <= 0) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        boolean admin = userService.isAdmin(loginUser);
        teamService.kickOut(teamId, userId, loginUser.getId(), admin);
        return ResultUtils.success("ok");
    }



}
