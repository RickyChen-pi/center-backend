package com.ricky.personcenter.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ricky.personcenter.common.BaseResponse;
import com.ricky.personcenter.common.ErrorCode;
import com.ricky.personcenter.common.ResultUtils;
import com.ricky.personcenter.error.BusinessException;
import com.ricky.personcenter.model.entity.Friends;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.model.request.FriendAddRequest;
import com.ricky.personcenter.model.vo.FriendsRecordVO;
import com.ricky.personcenter.service.FriendsService;
import com.ricky.personcenter.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 好友控制器
 *
 * @author Ricky
 * @date 2024/02/07
 */
@Slf4j
@RestController
@RequestMapping("/friends")
public class FriendsController {

    @Resource
    private FriendsService friendsService;

    @Resource
    private UserService userService;

    /**
     * 添加好友
     *
     * @param friendAddRequest 好友添加请求
     * @param request          请求
     * @return {@link BaseResponse}<{@link Boolean}>
     */
    @PostMapping("/add")
    public BaseResponse<Boolean> addFriends(@RequestBody FriendAddRequest friendAddRequest, HttpServletRequest request) {
        if (friendAddRequest == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR);
        }
        User loginUser = userService.getLoginUser(request);
        boolean addStatus = friendsService.addFriends(friendAddRequest, loginUser);
        return ResultUtils.success(addStatus);
    }

    /**
     * 查询添加好友的记录
     *
     * @param request 请求
     * @return {@link BaseResponse}<{@link List}<{@link FriendsRecordVO}>>
     */
    @GetMapping("/getRecords")
    public BaseResponse<List<FriendsRecordVO>> getRecords(HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        List<FriendsRecordVO> friendsList = friendsService.getRecords(loginUser);
        return ResultUtils.success(friendsList);
    }

    /**
     * 同意申请
     *
     * @param fromId  从 ID
     * @param request 请求
     * @return {@link BaseResponse}<{@link Boolean}>
     */
    @PostMapping("/agree/{fromId}")
    public BaseResponse<Boolean> agreeToApply(@PathVariable("fromId") Long fromId, HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        boolean agreeToApplyStatus = friendsService.agreeToApply(loginUser, fromId);
        return ResultUtils.success(agreeToApplyStatus);
    }

    /**
     * 拒绝申请
     *
     * @param fromId  从 ID
     * @param request 请求
     * @return {@link BaseResponse}<{@link Boolean}>
     */
    @PostMapping("/canceledApply/{id}")
    public BaseResponse<Boolean> canceledApply(@PathVariable("id") Long id, HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        boolean canceledApplyStatus = friendsService.canceledApply(loginUser, id);
        return ResultUtils.success(canceledApplyStatus);
    }

    /**
     * 按用户名搜索好友列表
     *
     * @param request 请求
     * @return {@link BaseResponse}<{@link List}<{@link User}>>
     */
    @GetMapping("/my/list")
    public BaseResponse<List<User>> searchUsersByUserName(HttpServletRequest request) {
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN);
        }
        Long userId = loginUser.getId();

        List<Friends> friendsList = friendsService.list(new QueryWrapper<Friends>()
                .eq("status", 1)
                .eq("receiveId", userId));

        List<User> userList = friendsList.stream()
                .map(friends -> userService.getById(friends.getFromId()))
                .distinct()
                .collect(Collectors.toList());
        return ResultUtils.success(userList);
    }



}
