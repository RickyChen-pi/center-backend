package com.ricky.personcenter.controller;

import com.ricky.personcenter.common.BaseResponse;
import com.ricky.personcenter.common.ErrorCode;
import com.ricky.personcenter.common.ResultUtils;
import com.ricky.personcenter.error.BusinessException;
import com.ricky.personcenter.model.entity.User;
import com.ricky.personcenter.service.FileService;
import com.ricky.personcenter.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * 文件控制器
 *
 * @author Ricky
 * @date 2024/02/12
 */
@Slf4j
@RestController
@CrossOrigin
@RequestMapping("/common")
public class FileAliOssController  {

    @Resource
    private UserService userService;
    @Resource
    private FileService ossService;

    @PostMapping("/upload")
    public BaseResponse<String> upload(MultipartFile file, HttpServletRequest request) {
        if (file == null) {
            throw new BusinessException(ErrorCode.PARAMS_ERROR, "请上传文件");
        }
        User loginUser = userService.getLoginUser(request);
        if (loginUser == null) {
            throw new BusinessException(ErrorCode.NOT_LOGIN, "请登录");
        }

        // 上传到阿里云对象存储
        String fileUrl = ossService.uploadFileAvatar(file);

        User user = new User();
        user.setId(loginUser.getId());
        user.setAvatarUrl(fileUrl);
        boolean success = userService.updateById(user);
        if (!success) {
            throw new BusinessException(ErrorCode.SYSTEM_ERROR, "头像上传失败");
        }
        return ResultUtils.success(fileUrl);
    }



}
