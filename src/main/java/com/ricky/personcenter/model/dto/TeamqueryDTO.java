package com.ricky.personcenter.model.dto;

import com.ricky.personcenter.common.PageRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.io.input.buffer.PeekableInputStream;

import java.util.List;

/**
 * TeamQuery DTO
 *
 * @author Ricky
 * @date 2024/01/15
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TeamqueryDTO  extends PageRequest {

    private static final long serialVersionUID = 4157915743062787850L;
    private Long id;

    private List<Long> idList;

    private String name;

    private String description;

    private Integer maxNum;

    private Long userId;

    private Integer status;

    /**
     * 搜索关键词（同时对队伍名称和描述搜索）
     */
    private String searchText;

    /**
     * 判断请求是否是搜索我请求的队伍和我创建的队伍，默认不是搜索这两者
     */
    private Boolean target = false;


}
