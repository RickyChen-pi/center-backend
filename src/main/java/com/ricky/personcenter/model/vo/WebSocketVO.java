package com.ricky.personcenter.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * Web 套接字 vo
 *
 * @author Ricky
 * @date 2024/02/11
 */
@Data
public class WebSocketVO implements Serializable {

    private static final long serialVersionUID = 5696443912817388879L;

    private long id;

    /**
     * 用户昵称
     */
    private String username;

    /**
     * 账号
     */
    private String userAccount;

    /**
     * 用户头像
     */
    private String avatarUrl;

}