package com.ricky.personcenter.model.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * 队伍的响应结果
 *
 * @author Ricky
 * @date 2024/02/08
 */
@Data
public class TeamVO implements Serializable {
    private static final long serialVersionUID = 1322528140329039539L;
    /**
     * id
     */
    private Long id;

    /**
     * 队伍名称
     */
    private String name;

    /**
     * 描述
     */
    private String description;

    /**
     * 封面图片
     */
    private String coverImage;

    /**
     * 最大人数
     */
    private Integer maxNum;

    /**
     * 已加入人数
     */
    private Integer joinNum;

    /**
     * 过期时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd") // 用于JSON序列化和反序列化
    @DateTimeFormat(pattern = "yyyy-MM-dd") // 用于数据绑定，例如从表单数据到Java对象
    private Date expireTime;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 0 - 公开，1 - 私有，2 - 加密
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 创建人用户信息
     */
    private UserVO createUser;


    private String leaderName;

    /**
     * 是否已加入队伍
     */
    private boolean hasJoin = false;

    private List<String> joinedUserAvatars;
}