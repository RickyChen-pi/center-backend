package com.ricky.personcenter.model.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 聊天消息 vo
 *
 * @author Ricky
 * @date 2024/02/11
 */
@Data
public class ChatMessageVO implements Serializable {
    /**
     * 串行版本uid
     */
    private static final long serialVersionUID = -5759086714391259663L;

    /**
     * 形式用户
     */
    private WebSocketVO fromUser;
    /**
     * 用户
     */
    private WebSocketVO toUser;
    /**
     * 团队id
     */
    private Long teamId;
    /**
     * 文本
     */
    private String text;
    /**
     * 是我
     */
    private Boolean isMy = false;
    /**
     * 聊天类型
     */
    private Integer chatType;
    /**
     * 是管理
     */
    private Boolean isAdmin = false;
    /**
     * 创建时间
     */
    private String createTime;
}