package com.ricky.personcenter.model.request;

import lombok.Data;

/**
 * 团队踢出请求
 *
 * @author Ricky
 * @date 2024/02/15
 */
@Data
public class TeamKickOutRequest {
    private Long teamId;
    private Long userId;
}