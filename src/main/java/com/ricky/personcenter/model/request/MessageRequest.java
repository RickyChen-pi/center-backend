package com.ricky.personcenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 消息请求
 *
 * @author Shier
 * @date 2023/06/19
 */
@Data
public class MessageRequest implements Serializable {
    /**
     * 串行版本uid
     */
    private static final long serialVersionUID = -1214317312892138887L;

    /**
     * 为id
     */
    private Long toId;
    /**
     * 团队id
     */
    private Long teamId;
    /**
     * 文本
     */
    private String text;
    /**
     * 聊天类型
     */
    private Integer chatType;
    /**
     * 是管理
     */
    private boolean isAdmin;
}