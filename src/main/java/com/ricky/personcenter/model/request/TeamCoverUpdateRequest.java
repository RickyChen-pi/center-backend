package com.ricky.personcenter.model.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * 团队封面更新请求
 *
 * @author Ricky
 * @date 2024/02/15
 */
@Data
public class TeamCoverUpdateRequest {
    private Long id;
    private MultipartFile file;
}