package com.ricky.personcenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户更新请求
 *
 * @author Ricky
 * @date 2024/02/12
 */
@Data
public class UserUpdateRequest implements Serializable {

    private static final long serialVersionUID = 6273644415215032694L;
    /**
     * 用户ID
     */
    private Long id;

    /**
     * 用户昵称
     */
    private String username;

    /**
     * 个人简介
     */
    private String profile;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 用户密码
     */
    private String password;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 标签列表
     */
    private String tags;

    private String code;

    private Integer role;
}