package com.ricky.personcenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 退出队伍请求
 *
 * @author Ricky
 * @date 2024/01/16
 */
@Data
public class TeamquitRequest implements Serializable {

    private static final long serialVersionUID = -4995626685528386637L;
    private Long teamId;

}
