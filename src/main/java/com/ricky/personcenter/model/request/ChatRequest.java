package com.ricky.personcenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 聊天请求
 *
 * @author Ricky
 * @date 2024/02/11
 */
@Data
public class ChatRequest implements Serializable {
    /**
     * 串行版本uid
     */
    private static final long serialVersionUID = 1445805872513828206L;

    /**
     * 队伍聊天室id
     */
    private Long teamId;

    /**
     * 接收消息id
     */
    private Long toId;

}