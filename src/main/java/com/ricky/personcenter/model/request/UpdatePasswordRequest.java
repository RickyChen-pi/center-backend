package com.ricky.personcenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 更新密码请求
 *
 * @author Ricky
 * @date 2024/02/13
 */
@Data
public class UpdatePasswordRequest implements Serializable {
    private static final long serialVersionUID = 4954058723983078157L;

    private String originPassword;
    private String newPassword;
    private String confirmPassword;

}
