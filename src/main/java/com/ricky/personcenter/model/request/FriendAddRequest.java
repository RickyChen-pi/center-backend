package com.ricky.personcenter.model.request;

import lombok.Data;

import java.io.Serializable;

/**
 * 好友添加请求
 *
 * @author Ricky
 * @date 2024/02/07
 */
@Data
public class FriendAddRequest implements Serializable {

    private static final long serialVersionUID = 3535200871417051166L;
    /**
     * id
     */
    private Long id;
    /**
     * 接收申请的用户id
     */
    private Long receiveId;

    /**
     * 好友申请备注信息
     */
    private String remark;
}
