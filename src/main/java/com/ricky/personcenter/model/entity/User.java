package com.ricky.personcenter.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author Ricky
 * @TableName user
 */
@TableName(value ="user")
@Data
public class User implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = -8728210164781350623L;
    /**
     * 用户编号
     */
    //@TableId(type = IdType.ASSIGN_ID)
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 昵称
     */
    private String username;

    /**
     * 登录账号
     */
    private String userAccount;

    /**
     * 头像
     */
    private String avatarUrl;

    /**
     * 性别
     */
    private Integer gender;

    /**
     * 密码
     */
    private String userPassword;

    /**
     * 电话
     */
    private String phone;

    /**
     * 个人简介
     */
    private String profile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户状态 0正常
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 更新时间
     */
    private Date updateTime;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 用户角色
     */
    private Integer role;


    /**
     * 标签列表
     */
    private String tags;

    /**
     * 好友 ID
     */
    private String friendIds;

}