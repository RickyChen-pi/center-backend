package com.ricky.personcenter.model.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @TableName 好友申请表
 */
@TableName(value ="friends")
@Data
public class Friends implements Serializable {
    /**
     * 主键
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 发送申请的好友id
     */
    private Long fromId;

    /**
     * 接收申请的用户id
     */
    private Long receiveId;

    /**
     * 0-未读，1-已读
     */
    private Integer isRead;

    /**
     * 申请转态，0-未通过，1-已同意，2-已过期，3-不同意
     */
    private Integer status;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDelete;

    /**
     * 好友申请备注信息
     */
    private String remark;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}