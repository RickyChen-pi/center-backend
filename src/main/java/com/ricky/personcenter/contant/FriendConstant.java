package com.ricky.personcenter.contant;

/**
 * 朋友常量
 *
 * @author Ricky
 * @date 2024/02/07
 */
public interface FriendConstant {

    /**
     * 已同意
     */
    int AGREE_STATUS = 1;
    /**
     * 已过期
     */
    int EXPIRED_STATUS = 2;

    /**
     * 撤销
     */
    int REVOKE_STATUS = 3;
    /**
     * 未读
     */
    int NOT_READ = 0;

    /**
     * 已读
     */
    int READ = 1;

    /**
     * 默认状态 未处理
     */
    int DEFAULT_STATUS = 0;
}