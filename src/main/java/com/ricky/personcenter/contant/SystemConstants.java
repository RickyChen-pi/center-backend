package com.ricky.personcenter.contant;

/**
 * 系统常量
 *
 * @author Ricky
 * @date 2024/02/16
 */
public final class SystemConstants {
    private SystemConstants() {
    }
    /**
     * 分页大小
     */
    public static final long PAGE_SIZE = 10;

    /**
     * 随机展示用户最低限度
     */
    public static final int MINIMUM_ENABLE_RANDOM_USER_NUM = 10;

    public static final int BUCKET_SIZE = 10000;
}
