package com.ricky.personcenter.contant;

/**
 * Redis 常量
 *
 * @author Ricky
 * @date 2024/02/15
 */
public final class RedisConstants {
    private RedisConstants() {
    }

    /**
     * 用户推荐缓存
     */
    public static final String USER_RECOMMEND_KEY = "personcenter:user:recommend:";

    /**
     * 最短缓存随机时间
     */
    public static final int MINIMUM_CACHE_RANDOM_TIME = 2;
    /**
     * 最大缓存随机时间
     */
    public static final int MAXIMUM_CACHE_RANDOM_TIME = 3;
    /**
     * 缓存时间偏移量
     */
    public static final int CACHE_TIME_OFFSET = 10;
}
