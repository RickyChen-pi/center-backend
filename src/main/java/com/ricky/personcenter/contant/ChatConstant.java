package com.ricky.personcenter.contant;

/**
 * 聊天常量
 *
 * @author Ricky
 * @date 2024/02/26
 */
public final class ChatConstant {
    private ChatConstant() {
    }

    /**
     * 私聊
     */
    public static final int PRIVATE_CHAT = 1;

    /**
     * 团队聊天
     */
    public static final int TEAM_CHAT = 2;

    /**
     * 大厅聊天
     */
    public static final int HALL_CHAT = 3;

    /**
     * 缓存聊天大厅
     */
    public static final String CACHE_CHAT_HALL = "personcenter:chat:chat_records:chat_hall";

    /**
     * 缓存聊天专用
     */
    public static final String CACHE_CHAT_PRIVATE = "personcenter:chat:chat_records:chat_private";

    /**
     *
     */
    public static final String CACHE_CHAT_TEAM = "personcenter:chat:chat_records:chat_team";

}
