package com.ricky.personcenter.contant;

/**
 * redisson常量
 *
 * @author Ricky
 * @date 2024/02/07
 */
public interface RedissonConstant {
    /**
     * 应用锁
     */
    String APPLY_LOCK = "personCenter:addFriend:lock:";

    String DISBAND_EXPIRED_TEAM_LOCK = "personCenter:disbandTeam:lock";
    String USER_RECOMMEND_LOCK = "personCenter:userRecommend:lock";
}