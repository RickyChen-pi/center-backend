//package com.ricky.personcenter;
//
//import cn.hutool.core.date.DateUtil;
//import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
//import com.ricky.personcenter.model.entity.Friends;
//import com.ricky.personcenter.model.entity.Team;
//import com.ricky.personcenter.model.vo.FriendsRecordVO;
//import com.ricky.personcenter.model.vo.UserVO;
//import com.ricky.personcenter.service.FriendsService;
//import com.ricky.personcenter.service.TeamService;
//import com.ricky.personcenter.service.UserService;
//import lombok.extern.slf4j.Slf4j;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import javax.annotation.Resource;
//import java.util.Date;
//import java.util.List;
//import java.util.Scanner;
//import java.util.stream.Collectors;
//
//@SpringBootTest
//@Slf4j
//public class test {
//    @Resource
//    private TeamService teamService;
//    @Resource
//    private UserService userService;
//    @Resource
//    private FriendsService friendsService;
//    @Test
//    void test(){
//
//        String currentTime = DateUtil.now();
//        List<Team> teamList = teamService.list();
//        for (Team team : teamList) {
//            Date expireTime = team.getExpireTime();
//            String format = DateUtil.format(expireTime, "yyyy-MM-dd HH:mm:ss");
//            if (DateUtil.parse(format).before(DateUtil.parse(currentTime))){
//                //如果数据库时间早于当前时间，则执行删除
//                teamService.deleteTeam(team.getId(), null, true);
//            }
//        }
//    }
//
//    @Test
//    void testUser(){
//        List<FriendsRecordVO> friendStatus = friendsService.getFriendStatus(3L, 9L);
//        LambdaQueryWrapper<Friends> friendsLambdaQueryWrapper = new LambdaQueryWrapper<>();
//        friendsLambdaQueryWrapper.eq(Friends::getFromId, 9L)
//                .eq(Friends::getReceiveId, 3L);
//        List<Friends> friendsList = friendsService.list(friendsLambdaQueryWrapper);
//
//        List<Friends> collect = friendsList.stream().distinct().collect(Collectors.toList());
//
//        for (Friends friendsRecordVO : collect) {
//            System.out.println(friendsRecordVO);
//        }
//
//        Scanner scanner = new Scanner(System.in);
//    }
//}
